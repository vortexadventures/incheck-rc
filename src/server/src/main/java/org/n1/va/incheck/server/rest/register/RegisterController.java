package org.n1.va.incheck.server.rest.register;

import org.n1.va.incheck.server.rest.register.jsonmodel.*;
import org.n1.va.incheck.server.rest.register.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest/JSON controller for Registration.
 */
@Controller
@RequestMapping("/register")
public class RegisterController {

    private Logger logger = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private RegisterService registerService;

    @Autowired
    private IncheckAndPaymentService incheckService;

    @Autowired
    PaymentHistoryService paymentHistoryService;

    @Autowired
    private MembershipService membershipService;

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private DeelnemerService deelnemerService;


    @ResponseBody
	@RequestMapping(value = "get/{plin}", method = RequestMethod.GET)
	public RegisterInfo getExtract(@PathVariable("plin") int plin) {
        try {
            RegisterInfo info = registerService.getRegistrationInfo(plin);
            return info;
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            return createError(e);
        }
	}

    private RegisterInfo createError(Exception e) {
        RegisterInfo info = new RegisterInfo();
        info.error = e;
        return info;
    }

    @ResponseBody
    @RequestMapping(value = "checkin/{plin}/{role}", method = RequestMethod.GET)
    public void checkIn(@PathVariable("plin") int plin, @PathVariable("role") String role, @CookieValue("user") String user) {
        try {
            incheckService.checkIn(user, plin, role);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "checkout/{plin}", method = RequestMethod.GET)
    public void checkOut(@PathVariable("plin") int plin) {
        try {
            incheckService.checkOut(plin);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "pay", method = RequestMethod.POST)
    public GenericResponse pay(@RequestBody PaymentMessage payment, @CookieValue("registerName") String kassa, @CookieValue("user") String user) {
        try {
            return incheckService.pay(user, payment, kassa);
        }
        catch (InvalidPaymentException invalid) {
            GenericResponse response = new GenericResponse(false, invalid.getMessage());
            return response;
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "paymentHistory/{plin}", method = RequestMethod.GET)
    public List<PaymentHistoryBoeking> paymentHistory(@PathVariable("plin") int plin) {
        try {
            return paymentHistoryService.getHistory(plin);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "changeMembership/{plin}", method = RequestMethod.GET)
    public String changeMembership(@PathVariable("plin") int plin) {
        try {
            return membershipService.changeMembership(plin);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "nextPlin/{plin}", method = RequestMethod.GET)
    public Integer nextPlin(@PathVariable("plin") int plin) {
        try {
            return currentEventService.nextPlinForEvent(plin);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "updateDob", method = RequestMethod.POST)
    public GenericResponse updateDob(@RequestBody JsonPlayerUpdate update) {
        try {
            return deelnemerService.updateDob(update);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "updateEmail", method = RequestMethod.POST)
    public GenericResponse updateEmail(@RequestBody JsonPlayerUpdate update) {
        try {
            return deelnemerService.updateEmail(update);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "new", method = RequestMethod.POST)
    public GenericResponseWithPlin updateEmail(@RequestBody JsonNewPlayer newPlayer) {
        try {
            return deelnemerService.createNew(newPlayer);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

//    @ResponseBody
//    @RequestMapping(value = "overview", method = RequestMethod.GET)
//    public Integer overview() {
//        List<Long> deelnemerIds = eventsRepo.getDeelnemersForEvent(2013, 2);
//        return deelnemerIds.size();
//    }
}