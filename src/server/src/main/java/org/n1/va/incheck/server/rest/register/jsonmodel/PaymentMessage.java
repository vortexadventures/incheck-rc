package org.n1.va.incheck.server.rest.register.jsonmodel;

import java.util.List;

/**
 * This is the top level json object that is sent when a payment is done
 */
public class PaymentMessage {

    public int plin;
    public String cashier;
    public  int totalAmount;
    public boolean cash;
    public List<PaymentItem> items;

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public boolean isCash() {
        return cash;
    }

    public void setCash(boolean cash) {
        this.cash = cash;
    }

    public List<PaymentItem> getItems() {
        return items;
    }

    public void setItems(List<PaymentItem> items) {
        this.items = items;
    }

    public int getPlin() {
        return plin;
    }

    public void setPlin(int plin) {
        this.plin = plin;
    }

    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }
}
