package org.n1.va.incheck.server.rest.notes;

import java.util.Date;

/**
 * A note / remark written with a player.
 */
public class Note {

    public Long id;
    public long deelnemer_id;
    public String tekst;
    public String gebruiker;
    public Date tijdstip;

    public Note() {
    }

    public Note(JsonNoteInput noteInput) {
        this.tekst = noteInput.text;
        this.gebruiker = noteInput.user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getDeelnemer_id() {
        return deelnemer_id;
    }

    public void setDeelnemer_id(long deelnemer_id) {
        this.deelnemer_id = deelnemer_id;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public String getGebruiker() {
        return gebruiker;
    }

    public void setGebruiker(String gebruiker) {
        this.gebruiker = gebruiker;
    }

    public Date getTijdstip() {
        return tijdstip;
    }

    public void setTijdstip(Date tijdstip) {
        this.tijdstip = tijdstip;
    }
}
