package org.n1.va.incheck.server.rest.register.service;

import org.n1.va.incheck.server.rest.register.jsonmodel.GenericResponse;
import org.n1.va.incheck.server.rest.register.jsonmodel.GenericResponseWithPlin;
import org.n1.va.incheck.server.rest.register.jsonmodel.JsonNewPlayer;
import org.n1.va.incheck.server.rest.register.jsonmodel.JsonPlayerUpdate;
import org.n1.va.incheck.server.rest.register.repo.DeelnemerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Service for updates to deelnemers
 */
@Service
public class DeelnemerService {

    @Autowired
    private DeelnemerRepo deelnemerRepo;

    private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");


    @Transactional
    public GenericResponse updateDob(JsonPlayerUpdate update) {
        try {
            Date dob = format.parse(update.input);
            deelnemerRepo.updateDob(update.plin, dob);
            return new GenericResponse(true, "Geboortedatu ge-update.");
        }
        catch (ParseException e) {
            return new GenericResponse(false, "Invoer onduidelijk. Gebruik: dd-mm-jjjj");
        }
    }

    @Transactional
    public GenericResponse updateEmail(JsonPlayerUpdate update) {
        deelnemerRepo.updateEmail(update.plin, update.input);
        return new GenericResponse(true, "Email ge-update.");
    }

    @Transactional
    public GenericResponseWithPlin createNew(JsonNewPlayer newPlayer) {
        if (newPlayer.tussenVoegsel== null){
            newPlayer.tussenVoegsel = "";
        }

        int plin = deelnemerRepo.getNextPlin();
        deelnemerRepo.createNewPlayer(plin, newPlayer);

        String message = "Deelnemer aangemaakt, PLIN: " +plin + " toegekend.";

        try {
            JsonPlayerUpdate update = new JsonPlayerUpdate();
            update.input = newPlayer.dob;
            update.plin = plin;
            GenericResponse response = updateDob(update);
            if (!response.success) {
                message += " Opslaan geboortedatum mislukt: " + response.message;
            }
        }
        catch (Exception e) {
            message += " Opslaan geboortedatum mislukt: " + e.getMessage();
        }
        try {
            JsonPlayerUpdate update = new JsonPlayerUpdate();
            update.input = newPlayer.email;
            update.plin = plin;
            GenericResponse response =  updateEmail(update);
            if (!response.success) {
                message += " Opslaan email mislukt: " + response.message;
            }
        }
        catch (Exception e) {
            message += " Opslaan email mislukt: " + e.getMessage();
        }

        return new GenericResponseWithPlin(true, message, plin);
    }
}
