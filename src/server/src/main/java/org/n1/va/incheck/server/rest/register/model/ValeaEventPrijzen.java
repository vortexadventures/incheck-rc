package org.n1.va.incheck.server.rest.register.model;

import org.joda.time.LocalDate;

import java.util.Date;

/** Event prijzen uit Valea. */
public class ValeaEventPrijzen {

    public int jaar;
    public int event;
    public double prijs_poort;
    private LocalDate prijs_voor_uiterlijke_datum;
    public double prijs_voor;
    public double prijs_crew;
    public double prijs_k12_15_voor;
    public double prijs_k12_15_poort;
    public double prijs_k11_jonger_voor;
    public double prijs_k11_jonger_poort;
    public double prijs_jaarpakket;
    public double prijs_jaarpakket_crew;

    /** Valea has no crew poortprijs, not initialized by default. Call initCrewPoortAndMonsterPrijs() to initialize. */
    public double prijs_crew_poort;

    /** Valea has no monster prijs or monster-poort prijs. Not initialized by default. Call initCrewPoortAndMonsterPrijs() to initialize. */
    public double prijs_monster;
    public double prijs_monster_poort;


    public double priceForEvent(ValeaEvent event, boolean voorInschrijving) {
        if (event.speciaal_bedrag) {
            throw new RuntimeException("Don't ask the price for a speciaalbedrag, it is not a fixed amount");
        }

        if (event.bestuur || event.ehbo || event.barlid ) {
            return 0.0d;
        }
        if (event.k12) {
            return prijs(voorInschrijving, prijs_k11_jonger_voor, prijs_k11_jonger_poort);
        }
        if (event.k16) {
            return prijs(voorInschrijving, prijs_k12_15_voor, prijs_k12_15_poort);
        }
        if (event.crew ) {
            initCrewPoortAndMonsterPrijs();
            return prijs(voorInschrijving, prijs_crew, prijs_crew_poort);
        }
        if ( event.monster) {
            initCrewPoortAndMonsterPrijs();
            return prijs(voorInschrijving, prijs_monster, prijs_monster_poort);
        }
        return prijs(voorInschrijving, prijs_voor, prijs_poort);
    }

    private double prijs(boolean voorInschrijving, double voorPrijs, double poortPrijs ) {
        if (voorInschrijving) {
            return voorPrijs;
        }
       return poortPrijs;
    }

    public boolean inTimeForVoorInschrijving(LocalDate paymentDate) {
        LocalDate gracePaymentDate = this.prijs_voor_uiterlijke_datum.plusDays(4);
        return paymentDate.isBefore(gracePaymentDate);
    }


    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public double getPrijs_poort() {
        return prijs_poort;
    }

    public void setPrijs_poort(double prijs_poort) {
        this.prijs_poort = prijs_poort;
    }

    public Date getPrijs_voor_uiterlijke_datum() {
        return prijs_voor_uiterlijke_datum.toDate();
    }

    public void setPrijs_voor_uiterlijke_datum(Date java_prijs_voor_uiterlijke_datum) {
        this.prijs_voor_uiterlijke_datum = new LocalDate(java_prijs_voor_uiterlijke_datum);
    }

    public double getPrijs_voor() {
        return prijs_voor;
    }

    public void setPrijs_voor(double prijs_voor) {
        this.prijs_voor = prijs_voor;
    }

    public double getPrijs_crew() {
        return prijs_crew;
    }

    public void setPrijs_crew(double prijs_crew) {
        this.prijs_crew = prijs_crew;
    }

    public double getPrijs_k12_15_voor() {
        return prijs_k12_15_voor;
    }

    public void setPrijs_k12_15_voor(double prijs_k12_15_voor) {
        this.prijs_k12_15_voor = prijs_k12_15_voor;
    }

    public double getPrijs_k12_15_poort() {
        return prijs_k12_15_poort;
    }

    public void setPrijs_k12_15_poort(double prijs_k12_15_poort) {
        this.prijs_k12_15_poort = prijs_k12_15_poort;
    }

    public double getPrijs_k11_jonger_voor() {
        return prijs_k11_jonger_voor;
    }

    public void setPrijs_k11_jonger_voor(double prijs_k11_jonger_voor) {
        this.prijs_k11_jonger_voor = prijs_k11_jonger_voor;
    }

    public double getPrijs_k11_jonger_poort() {
        return prijs_k11_jonger_poort;
    }

    public void setPrijs_k11_jonger_poort(double prijs_k11_jonger_poort) {
        this.prijs_k11_jonger_poort = prijs_k11_jonger_poort;
    }

    public double getPrijs_jaarpakket() {
        return prijs_jaarpakket;
    }

    public void setPrijs_jaarpakket(double prijs_jaarpakket) {
        this.prijs_jaarpakket = prijs_jaarpakket;
    }

    public double getPrijs_jaarpakket_crew() {
        return prijs_jaarpakket_crew;
    }

    public void setPrijs_jaarpakket_crew(double prijs_jaarpakket_crew) {
        this.prijs_jaarpakket_crew = prijs_jaarpakket_crew;
    }

    public void initCrewPoortAndMonsterPrijs() {
        // TODO: poortprijs crew not implemented, workaround: voorprijs + 5 euro.
        prijs_crew_poort = prijs_crew + 10.0d;

        // TODO: monsterprijs not implemented. workaround, change per event.
        prijs_monster = prijs_crew - 10.0d; // sum
        prijs_monster_poort = prijs_monster + 10.0d; // sum:
//        prijs_monster = prijs_crew - 15.0d; // moots
//        prijs_monster_poort = prijs_monster + 10.0d; // moots


    }
}
