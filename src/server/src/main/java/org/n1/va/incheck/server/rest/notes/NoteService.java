package org.n1.va.incheck.server.rest.notes;

import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.register.repo.DeelnemerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Service for managing notes.
 */
@Service
public class NoteService {

    @Autowired
    private NoteRepo noteRepo;

    @Autowired
    private DeelnemerRepo deelnemerRepo;

    public List<JsonNoteLine> addNote(JsonNoteInput noteInput) {
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(noteInput.plin);

        Note note = new Note(noteInput);
        note.tijdstip = new Date();
        note.deelnemer_id = deelnemer.id;

        noteRepo.insert(note);

        return getNotes(deelnemer);
    }

    public List<JsonNoteLine> getNotes(int plin) {
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(plin);
        return getNotes(deelnemer);
    }

    private List<JsonNoteLine> getNotes(ValeaDeelnemer deelnemer) {
        List<Note> notes =  noteRepo.getNotes(deelnemer);

        List<JsonNoteLine> jsonNotes = new ArrayList<>();
        for (Note note: notes) {
            JsonNoteLine jsonNote = new JsonNoteLine(note);
            jsonNotes.add(jsonNote);
        }

        if (deelnemer.opmerking != null && !deelnemer.opmerking.isEmpty()) {
            JsonNoteLine jsonNote = new JsonNoteLine();
            jsonNote.time = "vroeger";
            jsonNote.user = "Valea";
            jsonNote.text = deelnemer.opmerking;
            jsonNotes.add(jsonNote);
        }

        return jsonNotes;
    }
}
