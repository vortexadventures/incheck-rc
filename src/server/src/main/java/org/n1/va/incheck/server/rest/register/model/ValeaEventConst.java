package org.n1.va.incheck.server.rest.register.model;

/**
 */
public class ValeaEventConst {

    public static final int VERENIGINGSLIDMAATSCHAP = 0;
    public static final int SUMDAG = 1;
    public static final int MOOTS_1= 2;
    public static final int SUMMONING = 3;
    public static final int MOOTS_2 = 4;

    public static final String TOEGANG = "toegang";
    public static final String MEUBILAIR = "meubilair";
    public static final String CATERING = "catering";

    public static final String ROLE_CREW = "crew";
    public static final String ROLE_MONSTER = "monster";

    public static final String RC_ROLE_VERENIGINSLIDMAATSCHAP = "verenigingslidmaatschap";
    public static final String RC_ROLE_EVENTLIDMAATSCHAP = "eventlidmaatschap";
    public static final String RC_ROLE_PIN = "pin";

    public static final String KASSA_NAAM_PIN = "pin";

    public static String getEventNaam(int naam) {
        switch( naam) {
            case VERENIGINGSLIDMAATSCHAP:
                return "Verenigingslidmaatschap";
            case SUMDAG:
                return "Sumdag";
            case MOOTS_1:
                return "Moots 1";
            case SUMMONING:
                return "Summoning";
            case MOOTS_2:
                return "Moots 2";
            default:
                return "Onbekend event";
        }
    }
}
