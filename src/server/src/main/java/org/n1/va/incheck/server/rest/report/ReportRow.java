package org.n1.va.incheck.server.rest.report;


import org.n1.va.incheck.server.rest.util.SpaceUtil;

/**
 * Rows of the occupancy report
 */
public class ReportRow {

    public String plin;
    public String name;
    public String number;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPlin() {
        return plin;
    }

    public void setPlin(String plin) {
        this.plin = SpaceUtil.fourLeadingSpaces(plin);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
