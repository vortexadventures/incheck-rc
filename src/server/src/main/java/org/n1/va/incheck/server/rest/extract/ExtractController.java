package org.n1.va.incheck.server.rest.extract;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Rest/JSON controller for ExtractRow Extract
 */
@Controller
@RequestMapping("/extract")
public class ExtractController {

    @Autowired
    private ExtractRepo repo;

    @ResponseBody
	@RequestMapping(value = "all", method = RequestMethod.GET)
	public List<ExtractRow> getExtract() {
        try {
            List<ExtractRow> rows = repo.getExtract();
            return rows;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}
}