package org.n1.va.incheck.server.rest.register.service;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.achievements.AchievementService;
import org.n1.va.incheck.server.rest.cashregister.model.CashMutation;
import org.n1.va.incheck.server.rest.cashregister.repo.CashRegisterRepo;
import org.n1.va.incheck.server.rest.register.jsonmodel.*;
import org.n1.va.incheck.server.rest.register.model.*;
import org.n1.va.incheck.server.rest.register.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class defines the service that supports incheck
 */
@Service
public class IncheckAndPaymentService {

    Logger logger = LoggerFactory.getLogger(IncheckAndPaymentService.class);

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private DeelnemerRepo deelnemerRepo;

    @Autowired
    private EventsRepo eventsRepo;

    @Autowired
    private PaymentRepo paymentRepo;

    @Autowired
    private PaidForEventRepo paidForRepo;

    @Autowired
    private PrijzenRepo prijzenRepo;

    @Autowired
    private CashRegisterRepo cashRegisterRepo;

    @Autowired
    private AchievementService achievementService;


    @Transactional
    public void checkIn(String user, int plin, String role) {
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(plin);
        ValeaEvent event = getCurrentValeaEvent(deelnemer);

        if (event != null) {
            if (event.describeRole().equals(role)) {
                checkinForExistingEvent (event);
            }
            else {
                checkinForExistingEventWithNewRole(event, role);
            }
            achievementService.processCheckin(user, plin);
            return;
        }
        else {
            ValeaEvent persistedEvent = createNewEvent(deelnemer, role, currentEventService.event.eventName);
            checkinForExistingEvent(persistedEvent);
            achievementService.processCheckin(user, plin);
        }
    }

    private void checkinForExistingEventWithNewRole(ValeaEvent event, String role) {
        event.updateForNewRole(role);
        eventsRepo.updateEventPartial(event);
        checkinForExistingEvent(event);
    }

    private void checkinForExistingEvent(ValeaEvent event) {
        if (event.uitcheck_date != null) {
            eventsRepo.updateCheckout(event, null);
        }
        else {
            eventsRepo.updateCheckin(event, new Date());
        }
    }

    private ValeaEvent createNewEvent(ValeaDeelnemer deelnemer, String role, int eventNaam) {
        ValeaEvent newEvent = new ValeaEvent();
        newEvent.deelnemer_id = deelnemer.id;
        newEvent.jaar = currentEventService.event.jaar;
        newEvent.naam = eventNaam;
        newEvent.updateForNewRole(role);

        eventsRepo.createEvent(newEvent);
        return newEvent;
    }

    @Transactional
    public void checkOut(int plin) {
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(plin);
        ValeaEvent event = getCurrentValeaEvent(deelnemer);

        if (event.incheck_date == null) {
            throw new RuntimeException("Persoon (plin=" + plin + ") was niet ingechecked en kan dus niet uitchecken.");
        }
        LocalDate incheckDate = new LocalDate(event.incheck_date);
        LocalDate now = LocalDate.now();
        eventsRepo.updateCheckout(event, new Date());
    }

    private ValeaEvent getCurrentValeaEvent(ValeaDeelnemer deelnemer) {
        int jaar = currentEventService.event.jaar;
        int eventName = currentEventService.event.eventName;
        return eventsRepo.getEventDataForPerson(deelnemer, jaar, eventName);
    }

    @Transactional
    public GenericResponse pay(String user, PaymentMessage payment, String kassa) {
        validateAmount(payment);

        double amountCashWithdrawnWithPin = processCashWithdrawlItems(payment);
        String role = determineAndValidateRoles(payment);

        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(payment.plin);

        ValeaEvent lidmaatschapsEvent = handlePossibleVerenigingslidmaatschapPayment(payment, deelnemer);

        ValeaEvent event = getCurrentValeaEvent(deelnemer);
        if (event == null) {
            if (role == null) {
                role = "speler";
            }
            event = createNewEvent(deelnemer, role, currentEventService.event.eventName);
        }

        ValeaBankBoeking boeking = createBoeking(payment, deelnemer);
        paymentRepo.insertPayment(boeking);
        insertAssociatedCashMutations(payment, boeking, kassa, amountCashWithdrawnWithPin);

        if(boeking.bedrag == 0) {
            return new GenericResponse(true, "Pintrasnasctie van " + amountCashWithdrawnWithPin + " voor " + deelnemer.name + " verwerkt.");
        }

        for (PaymentItem item: payment.items) {
            ValeaBoekingMapping mapping = createBoekingMapping(item, event, boeking, lidmaatschapsEvent, deelnemer);
            paymentRepo.insertMapping(mapping);
        }

        if (role != null && !role.equals(event.getRoleCode())) {
            eventsRepo.updateEventPartial(event);
        }

        logger.info("paid. boekingId = " + boeking.id);
        achievementService.processPayment(user, (payment.totalAmount / 100.0d));
        return new GenericResponse(true, "Betaling van " + (payment.totalAmount / 100.0d) + " voor " + deelnemer.name + " verwerkt.");
    }

    private double processCashWithdrawlItems(PaymentMessage payment) {
        List<PaymentItem> nonPinItems = new ArrayList<>();

        int totalCashWithdrawnCents = 0;
        int newTotalBoekingCents  = 0;

        for (PaymentItem item: payment.items ) {
            if (ValeaEventConst.RC_ROLE_PIN.equals(item.role)) {
                totalCashWithdrawnCents += item.total;
            }
            else {
                nonPinItems.add(item);
                newTotalBoekingCents += item.total;
            }
        }
        payment.items = nonPinItems;
        payment.totalAmount = newTotalBoekingCents;

        return (totalCashWithdrawnCents / 100.0d);
    }

    private void insertAssociatedCashMutations(PaymentMessage payment, ValeaBankBoeking boeking, String kassa, double amountCashWithdrawnWithPin) {
        CashMutation mutation = new CashMutation();

        if (payment.cash) {
            mutation.kassa = kassa;
        }
        else {
            mutation.kassa = ValeaEventConst.KASSA_NAAM_PIN;
        }
        mutation.bedrag = boeking.bedrag + amountCashWithdrawnWithPin;
        mutation.kasboeking_id = boeking.id;
        mutation.uitgevoerd_door = payment.cashier;
        mutation.omschrijving = boeking.omschrijving1;
        mutation.tijdstip = new Date();

        cashRegisterRepo.insert(mutation);

        if (amountCashWithdrawnWithPin <= 0) {
            return;
        }

        CashMutation withdrawlMutation = new CashMutation();
        withdrawlMutation.kassa = kassa;
        withdrawlMutation.bedrag = -amountCashWithdrawnWithPin;
        withdrawlMutation.kasboeking_id = null;
        withdrawlMutation.uitgevoerd_door = payment.cashier;
        withdrawlMutation.omschrijving = "Geld gepinned bij: " + boeking.omschrijving1;
        withdrawlMutation.tijdstip = new Date();
        cashRegisterRepo.insert(withdrawlMutation);
    }

    private void validateAmount(PaymentMessage payment) {
        if (payment.totalAmount == 0.0d) {
            throw new RuntimeException("Payment for no amount received");
        }
    }

    private ValeaEvent handlePossibleVerenigingslidmaatschapPayment(PaymentMessage payment, ValeaDeelnemer deelnemer) {
        ValeaEvent handledEvent = null;
        for (PaymentItem item: payment.items) {
            if (ValeaEventConst.RC_ROLE_VERENIGINSLIDMAATSCHAP.equals(item.role)) {
                if (handledEvent != null) {
                    throw new InvalidPaymentException("Meerdere verenigingslidmaatschappen gevonden.");
                }
                handledEvent = handleVerenigingslidmaatschapPayment(item, deelnemer);
            }
        }
        return handledEvent;
    }

    private ValeaEvent handleVerenigingslidmaatschapPayment(PaymentItem item, ValeaDeelnemer deelnemer) {
        ValeaEvent membershipEvent = eventsRepo.getEventDataForPerson(deelnemer, currentEventService.event.jaar, ValeaEventConst.VERENIGINGSLIDMAATSCHAP);
        if (membershipEvent != null) {
            double amountPaidAlready = paidForRepo.getPaidForEvent(membershipEvent);
            if (amountPaidAlready > 0) {
                double amountDue = prijzenRepo.getJaarPrijzen(currentEventService.event.jaar).jaarlidmaatschap - amountPaidAlready;
                throw new InvalidPaymentException("Er is al verenigingslidmaatschap betaald, maar niet genoeg. Er mist: " + amountDue +
                        " . Helaas kan de kassa deze situatie nog niet verwerken. Doe een handmatige betaling voor het ontbrekende bedrag, " +
                        "en maak een melding van de situatie, dan fixen we het achteraf.");
            }
            membershipEvent.evenementlidmaatschap_betaald = true;
            eventsRepo.updateEventPartial(membershipEvent);
        }
        else {
            membershipEvent = new ValeaEvent();
            membershipEvent.deelnemer_id = deelnemer.id;
            membershipEvent.naam = ValeaEventConst.VERENIGINGSLIDMAATSCHAP;
            membershipEvent.jaar = currentEventService.event.jaar;
            membershipEvent.evenementlidmaatschap_betaald = true;

            eventsRepo.createEvent(membershipEvent);
        }
        return membershipEvent;
    }

    private ValeaBoekingMapping createBoekingMapping(PaymentItem item, ValeaEvent event, ValeaBankBoeking boeking, ValeaEvent lidmaatschapsEvent, ValeaDeelnemer deelnemer) {
        ValeaBoekingMapping mapping = new ValeaBoekingMapping();
        mapping.bedrag = item.total / 100.0d;
        mapping.betaald_voor = item.purpose;
        mapping.boekingId = boeking.id;
        if (ValeaEventConst.RC_ROLE_VERENIGINSLIDMAATSCHAP.equals(item.role)) {
            mapping.eventId = lidmaatschapsEvent.id;
        }
        else {
            mapping.eventId = event.id;
        }
        mapping.omschrijving = item.name;
        mapping.product = item.role;

        if ( item.getName().equals("Jaarpakket")) {
            return processJaarpakket(item, event, boeking, mapping, deelnemer);
        }

        return mapping;
    }

    private ValeaBoekingMapping processJaarpakket(PaymentItem item, ValeaEvent event_m1, ValeaBankBoeking boeking, ValeaBoekingMapping mapping_m1, ValeaDeelnemer deelnemer) {
        ValeaEvent event_sum = getOrCreateEvent(deelnemer, ValeaEventConst.SUMMONING);
        ValeaEvent event_m2 = getOrCreateEvent(deelnemer, ValeaEventConst.MOOTS_2);

        ValeaBoekingMapping mapping_sum = createMappingForEvent(event_sum, 70.0, "toegang", boeking, "Jaarpakket", "speler");
        ValeaBoekingMapping mapping_m2 = createMappingForEvent(event_m2, 47.50, "toegang", boeking, "Jaarpakket", "speler");
        mapping_m1.bedrag = 47.50;

        double eventsTotal = mapping_m1.bedrag + mapping_sum.bedrag + mapping_m2.bedrag;
        if ( eventsTotal != item.total / 100d) {
            throw new RuntimeException("Dividing the jaarpakket amount over 3 events failed. Item amount = " + item.total + ", events total = " + eventsTotal );
        }

        paymentRepo.insertMapping(mapping_sum);
        paymentRepo.insertMapping(mapping_m2);

        return mapping_m1;
    }

    private ValeaBoekingMapping createMappingForEvent(ValeaEvent event, double amount, String betaald_voor, ValeaBankBoeking boeking, String description, String product) {
        ValeaBoekingMapping mapping = new ValeaBoekingMapping();
        mapping.bedrag = amount;
        mapping.betaald_voor = betaald_voor;
        mapping.boekingId = boeking.id;
        mapping.eventId = event.id;
        mapping.omschrijving = description;
        mapping.product = product;

        return mapping;
    }


    private ValeaEvent getOrCreateEvent(ValeaDeelnemer deelnemer, int eventNaam) {
        ValeaEvent event = eventsRepo.getEventDataForPerson(deelnemer, currentEventService.event.jaar, eventNaam);
        if (event == null) {
            event = createNewEvent(deelnemer, "speler", eventNaam);
        }
        return event;
    }

    private String determineAndValidateRoles(PaymentMessage payment) {
        String roleFound = null;
        String roleFoundDescription = null;

        String membershipFound = null;
        String membershipFoundDescription = null;

        ValeaEvent tempEvent = new ValeaEvent();

        for (PaymentItem item: payment.items) {
            if (item.purpose.equals("toegang") ) {
                if (tempEvent.updateForRoleLenient(item.role)) {
                    // valid role

                    if ( roleFound != null ) {
                        throw new InvalidPaymentException("Betaling bevat tegenstrijdige elementen. Betaling voor \"" +
                                roleFoundDescription + "\" en \"" + item.name + "\".");
                    }
                    else {
                        roleFound = item.role;
                        roleFoundDescription  = item.name;
                    }
                }
                else {
                    if (ValeaEventConst.RC_ROLE_EVENTLIDMAATSCHAP.equals(item.role) || ValeaEventConst.RC_ROLE_VERENIGINSLIDMAATSCHAP.equals(item.role)) {
                        if (membershipFound != null) {
                            throw new InvalidPaymentException("Betaling bevat tegenstrijdige elementen. Betaling voor \"" + membershipFoundDescription +
                                    "\" and \"" + item.name + "\"");
                        }
                        else {
                            membershipFound = item.role;
                            membershipFoundDescription = item.name;
                        }
                    }
                }
            }
        }
        return roleFound;
    }

    private ValeaBankBoeking createBoeking(PaymentMessage message, ValeaDeelnemer deelnemer) {
        ValeaBankBoeking boeking = new ValeaBankBoeking();

        boeking.bedrag = message.totalAmount / 100.0d;
        boeking.datum = new Date();
        boeking.kassa = "rc";
        boeking.omschrijving1 = "Poortbetaling ";
        if (message.cash) {
            boeking.omschrijving1 += "cash";
        }
        else {
            boeking.omschrijving1 += "PIN";
        }
        boeking.omschrijving1 += " voor plin " + message.plin + " - " + deelnemer.name;
        boeking.omschrijving2 = message.cashier.toLowerCase().trim();
        return boeking;
    }



}
