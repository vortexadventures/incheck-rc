package org.n1.va.incheck.server.rest.cashregister.model;

import java.util.Date;

/**
 * A mutation in a cash register
 */
public class CashMutation {

    public Long id;
    public String kassa;
    public double bedrag;
    public String omschrijving;
    public String uitgevoerd_door;
    public Date tijdstip;
    public Long kasboeking_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKassa() {
        return kassa;
    }

    public void setKassa(String kassa) {
        this.kassa = kassa;
    }

    public double getBedrag() {
        return bedrag;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public String getUitgevoerd_door() {
        return uitgevoerd_door;
    }

    public void setUitgevoerd_door(String uitgevoerd_door) {
        this.uitgevoerd_door = uitgevoerd_door;
    }

    public Date getTijdstip() {
        return tijdstip;
    }

    public void setTijdstip(Date tijdstip) {
        this.tijdstip = tijdstip;
    }

    public Long getKasboeking_id() {
        return kasboeking_id;
    }

    public void setKasboeking_id(Long kasboeking_id) {
        this.kasboeking_id = kasboeking_id;
    }
}
