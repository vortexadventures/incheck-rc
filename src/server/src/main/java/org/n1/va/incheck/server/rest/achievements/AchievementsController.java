package org.n1.va.incheck.server.rest.achievements;

import org.n1.va.incheck.server.rest.achievements.achievement.AchievementAward;
import org.n1.va.incheck.server.rest.notes.JsonNoteInput;
import org.n1.va.incheck.server.rest.notes.JsonNoteLine;
import org.n1.va.incheck.server.rest.notes.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest/JSON controller for Notes.
 */
@Controller
@RequestMapping("/achievements")
public class AchievementsController {

    private Logger logger = LoggerFactory.getLogger(AchievementsController.class);

    @Autowired
    private AchievementService achievementService;

    @ResponseBody
	@RequestMapping(value = "get", method = RequestMethod.GET)
	public AchievementAward getNotes(@CookieValue("user") String user) {
        try {
            return achievementService.getPossibleAchievement(user);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
	}
}