package org.n1.va.incheck.server.rest.util;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Minimal statement logger.
 */
public class RedoLogger {

    private Logger logger = LoggerFactory.getLogger(RedoLogger.class);

    public static final RedoLogger INSTANCE = new RedoLogger();

    private int lastMinute = 0;

    private RedoLogger() {
    }

    public void redoLog(String sql, Object[] args) {
        String params = createParamsString(args);
        writeLog(sql, params);
    }

    public void redoLog(String sql, Map<String,?> paramMap) {
        String params = createParamsString(paramMap);
        writeLog(sql, params);
    }

    private String createParamsString(Map<String, ?> paramMap) {
        StringBuilder builder = new StringBuilder();
        for (String key: paramMap.keySet()) {
            Object value = paramMap.get(key);
            builder.append(":");
            builder.append(key);
            builder.append("=");
            builder.append("\"");
            String val = quoteEscape(value);
            builder.append(val);
            builder.append("\",");
        }
        return builder.substring(0, builder.length() -1);
    }
    private synchronized void writeLog(String sql, String paramsLine) {
        String fileName = calcFileName();
//        writeLog("E:/redo/" + fileName + ".sql", sql, paramsLine);
//        writeLog("C:/redo/" + fileName + ".sql", sql, paramsLine);
    }

    private void writeLog(String fileName, String sql, String paramsLine) {
        try {

            FileWriter writer = new FileWriter(fileName, true);
            PrintWriter print = new PrintWriter(writer);
            print.println(sql);
            print.println(paramsLine);
            writer.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException("Failed to write redo log file due to " + e.getMessage());
        }
    }

    private String calcFileName() {
        LocalDateTime now = LocalDateTime.now();
        String text = now.toString("yyyyMMdd-HHmm");
        return "redo-"+text;
    }


    private String createParamsString(Object[] args) {
        StringBuilder builder = new StringBuilder();
        for (Object arg: args) {
            builder.append("\"");
            String val = quoteEscape(arg);
            builder.append(val);
            builder.append("\",");
        }
        return builder.substring(0, builder.length() -1);
    }

    private String quoteEscape(Object input) {
        StringBuilder builder = new StringBuilder();
        String value = (input==null) ? "#null#" : input.toString();
        String[] parts = value.split("\"");
        boolean frist = true;
        for (String part: parts) {
            if (!frist) {
                builder.append("\\\"");
            }
            else {
                frist = false;
            }
            builder.append(part);
        }
        return builder.toString();
    }


}
