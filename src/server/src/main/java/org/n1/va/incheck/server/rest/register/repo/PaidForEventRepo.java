package org.n1.va.incheck.server.rest.register.repo;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.register.model.ValeaEvent;
import org.n1.va.incheck.server.rest.register.model.ValeaEventConst;
import org.n1.va.incheck.server.rest.register.model.ValeaEventPrijzen;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PaidForEventRepo extends AbstractRepo {

    @Autowired
    public PaidForEventRepo(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * Find total payment for this event (for 'toegang').
     */
    public double getPaidForEvent(ValeaEvent event) {
        List<Double> paid = this.template.query(
            "select sum(bedrag) as totaal from boeking_event where event_id = ? and betaald_voor='toegang'",
                new ParameterizedRowMapper<Double>() {
                    public Double mapRow(ResultSet rs, int row) throws SQLException {
                        return rs.getDouble("totaal");
                    }
                },
                event.id);

        if (paid.isEmpty()) {
            return 0.0d;
        }
        return paid.get(0);
    }

    /**
     * Find date of first payment associated with this event.
     */
    public LocalDate getPaymentDate(ValeaEvent event) {
        List<Date> paymentDates = this.template.query(
                "select datum from kasboeking, boeking_event " +
                        "where event_id = ? " +
                        "and betaald_voor='toegang' " +
                        "and kasboeking.id = boeking_event.boeking_id " +
                        "order by datum",
                new ParameterizedRowMapper<Date>() {
                    public Date mapRow(ResultSet rs, int row) throws SQLException {
                        return rs.getDate("datum");
                    }
                },
                event.id);

        if (paymentDates.isEmpty()) {
            return new LocalDate();
        }
        long millis = paymentDates.get(0).getTime();
        return new LocalDate(millis);
    }

    public boolean determineIfPaidByJaarPakket(ValeaEvent event) {
         List<Long> payments = this.template.query(
                "select kasboeking.id from boeking_event, kasboeking" +
                        " where boeking_event.event_id=?" +
                        " and kasboeking.id = boeking_event.boeking_id",
                new ParameterizedRowMapper<Long>() {
                    public Long mapRow(ResultSet rs, int row) throws SQLException {
                        return rs.getLong("id");
                    }
                },
               event.id);

        for (Long paymentId: payments) {
            if (paymentPaysForYearPackage(paymentId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if this payment pays for MOOTS_1, SUM and MOOTS_2.
     */
    private boolean paymentPaysForYearPackage(Long paymentId) {
        List<Integer> boekingEvents = this.template.query(
                "select boeking_event.betaald_voor, events.naam " +
                        "from boeking_event, events " +
                        "where boeking_event.boeking_id = ? " +
                        "and events.id = boeking_event.event_id",
                new ParameterizedRowMapper<Integer>() {
                    public Integer mapRow(ResultSet rs, int row) throws SQLException {
                        String purpose = rs.getString("betaald_voor");
                        if (ValeaEventConst.TOEGANG.equalsIgnoreCase(purpose)) {
                            return rs.getInt("naam");
                        }
                        else {
                            return null;
                        }
                    }
                },
                paymentId);

        return boekingEvents.contains(ValeaEventConst.MOOTS_1) &&
                boekingEvents.contains(ValeaEventConst.SUMMONING) &&
                boekingEvents.contains(ValeaEventConst.MOOTS_2);
    }

    public double findTotalAmountPaidForThisYear(ValeaDeelnemer deelnemer, int year) {
        double total = this.template.queryForObject(
                "select sum(boeking_event.bedrag) as total " +
                        "from boeking_event, events " +
                        "where events.id = boeking_event.event_id " +
                        "and events.deelnemer_id = ? " +
                        "and boeking_event.betaald_voor='toegang' " +
                        "and events.jaar=? " +
                        "and events.naam != 1",
                Double.class,
                deelnemer.id, year);
        return total;
    }
}

