package org.n1.va.incheck.server.rest.cashregister;

import org.joda.time.LocalDateTime;
import org.n1.va.incheck.server.rest.cashregister.model.JsonCashMutationInput;
import org.n1.va.incheck.server.rest.cashregister.model.JsonCashMutationReport;
import org.n1.va.incheck.server.rest.cashregister.service.CashRegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;

/**
 * Rest/JSON controller for Registration.
 */
@Controller
@RequestMapping("/cashregister")
public class CashRegisterController {

    private Logger logger = LoggerFactory.getLogger(CashRegisterController.class);

    @Autowired
    private CashRegisterService cashRegisterService;


    @ResponseBody
	@RequestMapping(value = "report", method = RequestMethod.GET)
	public JsonCashMutationReport getReport(@CookieValue("registerName") String kassa) {
        try {
            return cashRegisterService.getMutationsReport(kassa);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
	}

    @ResponseBody
    @RequestMapping(value = "mutate", method = RequestMethod.POST)
    public void mutate(@RequestBody JsonCashMutationInput jsonMutation, @CookieValue("registerName") String kassa) {
        try {
            cashRegisterService.createMutation(jsonMutation, kassa);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "check", method = RequestMethod.POST)
    public void check(@RequestBody String registerName, @CookieValue("registerName") String registerCookieName) {
        try {
            if (registerCookieName.equals(registerName)) {
                return;
            }
            else {
                logger.error("Mismatch between cashregister cookies. Cookie: \"" + registerCookieName +
                        "\", postName: \"" + registerName + "\"");
                throw new RuntimeException();
            }
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
