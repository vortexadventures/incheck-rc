package org.n1.va.incheck.server.rest.register.jsonmodel;


/**
 * Overview of whether a person has paid in full.
 */
public class RegisterPaymentBooleans {

    public boolean event;
    public boolean membership;

    public RegisterPaymentBooleans() {
    }
    
    public RegisterPaymentBooleans(boolean event, boolean membership) {
        this.event = event;
        this.membership = membership;
    }

    public boolean isEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }

    public boolean getMembership() {
        return membership;
    }

    public void setMembership(boolean membership) {
        this.membership = membership;
    }
}
