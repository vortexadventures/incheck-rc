package org.n1.va.incheck.server.rest.report;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.model.ValeaEventPrijzen;
import org.n1.va.incheck.server.rest.register.repo.PrijzenRepo;
import org.n1.va.incheck.server.rest.register.service.CurrentEventService;
import org.n1.va.incheck.server.rest.util.SpaceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Rest/JSON controller for ExtractRow Extract
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportRepo repo;

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private PrijzenRepo prijzenRepo;

	@RequestMapping(value = "occupants/{date}", method = RequestMethod.GET)
	public String getExtract(@PathVariable("date") String dateInput, Map<String, Object> model) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date reportDate = format.parse(dateInput);

            model.put("parsedDate", format.format(reportDate));
            List<ReportRow> rows = repo.getOccupants(reportDate, currentEventService.event);
            model.put("contestants", rows);

            int number = 1;
            for (ReportRow row: rows) {
                row.number = SpaceUtil.fourLeadingSpaces("" + number);
                number +=1 ;
            }
        }
        catch (ParseException e) {
            model.put("errorMessage", e.getMessage());
        }
        return "occupantsReport";
	}
    @RequestMapping(value = "cashregistermutations", method = RequestMethod.GET)
    public String getExtract(Map<String, Object> model) {

        ValeaEventPrijzen prijzen = prijzenRepo.getPrijzen(currentEventService.event);
        Date startDate = prijzen.getPrijs_voor_uiterlijke_datum();

        model.put("start", new LocalDate(startDate));
        model.put("end", new LocalDate());

        List<MutationRow> mutations = repo.getCashRegisterMutations(startDate);
        MutationRow header = createHeader();
        mutations.add(0, header);
        model.put("mutations", mutations);

        return "cashregistermutationsReport";
    }

    private MutationRow createHeader() {
        MutationRow row = new MutationRow();
        row.bedrag = "\"Bedrag\"";
        row.gebruiker="\"Uitvoering\"";
        row.tijd="Tijd";
        row.dag="Datum";
        row.omschrijving="Omschrijving";
        row.kassa="Kassa";

        return row;
    }


}