package org.n1.va.incheck.server.rest.notes;

/**
 * Incoming note
 */
public class JsonNoteInput {
    public String text;
    public String user;
    public int plin;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPlin() {
        return plin;
    }

    public void setPlin(int plin) {
        this.plin = plin;
    }
}
