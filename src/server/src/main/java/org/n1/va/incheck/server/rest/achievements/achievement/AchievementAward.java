package org.n1.va.incheck.server.rest.achievements.achievement;

/**
 *
 */
public class AchievementAward {

    public String image;
    public String message;

    public AchievementAward(String image, String message) {
        this.image = image;
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
