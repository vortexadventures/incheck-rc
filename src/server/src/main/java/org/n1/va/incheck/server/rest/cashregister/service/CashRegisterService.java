package org.n1.va.incheck.server.rest.cashregister.service;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.n1.va.incheck.server.rest.cashregister.model.CashMutation;
import org.n1.va.incheck.server.rest.cashregister.model.JsonCashMutation;
import org.n1.va.incheck.server.rest.cashregister.model.JsonCashMutationInput;
import org.n1.va.incheck.server.rest.cashregister.model.JsonCashMutationReport;
import org.n1.va.incheck.server.rest.cashregister.repo.CashRegisterRepo;
import org.n1.va.incheck.server.rest.register.model.ValeaEventPrijzen;
import org.n1.va.incheck.server.rest.register.repo.PrijzenRepo;
import org.n1.va.incheck.server.rest.register.service.CurrentEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Service for CashRegister.
 */
@Service
public class CashRegisterService {

    @Autowired
    private CashRegisterRepo repo;

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private PrijzenRepo prijzenRepo;

    private Map<Integer, String> weekdagenMap = new HashMap<>();

    public CashRegisterService() {
        weekdagenMap.put(DateTimeConstants.MONDAY, "ma");
        weekdagenMap.put(DateTimeConstants.TUESDAY, "di");
        weekdagenMap.put(DateTimeConstants.WEDNESDAY, "wo");
        weekdagenMap.put(DateTimeConstants.THURSDAY, "do");
        weekdagenMap.put(DateTimeConstants.FRIDAY, "vr");
        weekdagenMap.put(DateTimeConstants.SATURDAY, "za");
        weekdagenMap.put(DateTimeConstants.SUNDAY, "do");
    }

    @Transactional
    public void createMutation(JsonCashMutationInput jsonMutation, String kassa) {
        CashMutation mutation = new CashMutation();

        mutation.tijdstip = new Date();
        mutation.bedrag = jsonMutation.amount;
        mutation.kassa = kassa;
        mutation.uitgevoerd_door = jsonMutation.user;
        mutation.omschrijving = jsonMutation.reason;
        mutation.kasboeking_id = null;

        repo.insert(mutation);
    }

    public JsonCashMutationReport getMutationsReport(String kassa) {

        ValeaEventPrijzen prijzen = prijzenRepo.getPrijzen(currentEventService.event);
        Date startDate = prijzen.getPrijs_voor_uiterlijke_datum();

        List<JsonCashMutation> mutations = getMutations(startDate, kassa);
        JsonCashMutationReport report = new JsonCashMutationReport();
        report.mutations = mutations;
        report.total= repo.getTotal(startDate, kassa);

        return report;
   }


    private List<JsonCashMutation> getMutations(Date cutoff, String kassa) {
        List<JsonCashMutation> jsonMutations = new ArrayList<>();

        List<CashMutation> mutations = repo.getMutations(cutoff, kassa);

        for (CashMutation mutation: mutations) {
            JsonCashMutation jsonMutation = new JsonCashMutation();
            jsonMutation.amount = mutation.bedrag;
            jsonMutation.date = mutation.tijdstip;
            jsonMutation.user = mutation.uitgevoerd_door;
            jsonMutation.reason = mutation.omschrijving;
            jsonMutation.showDate = describeDate(mutation.tijdstip);

            jsonMutations.add(jsonMutation);
        }
        return jsonMutations;
    }

    private String describeDate(Date tijdstip) {
        LocalDateTime date = new LocalDateTime(tijdstip);

        int weekdayIndex = date.getDayOfWeek();

        String time = date.toString("HH:mm");
        String weekDag = parseWeekdag(weekdayIndex);

        return weekDag + " " + time;
    }

    private String parseWeekdag(int weekdayIndex) {
        return weekdagenMap.get(weekdayIndex);
    }

}
