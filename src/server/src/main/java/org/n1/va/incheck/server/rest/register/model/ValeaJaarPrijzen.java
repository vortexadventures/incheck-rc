package org.n1.va.incheck.server.rest.register.model;

import java.sql.Date;

/**
 * Prices that govern a whole year.
 */
public class ValeaJaarPrijzen {
    public int jaar;
    public double jaarpakket;
    public double crew_jaarpakket;
    public double evenementlidmaatschap;
    public double jaarlidmaatschap;
    public Date jaarpakket_uiterlijke_datum;

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public double getJaarpakket() {
        return jaarpakket;
    }

    public void setJaarpakket(double jaarpakket) {
        this.jaarpakket = jaarpakket;
    }

    public double getCrew_jaarpakket() {
        return crew_jaarpakket;
    }

    public void setCrew_jaarpakket(double crew_jaarpakket) {
        this.crew_jaarpakket = crew_jaarpakket;
    }

    public double getEvenementlidmaatschap() {
        return evenementlidmaatschap;
    }

    public void setEvenementlidmaatschap(double evenementlidmaatschap) {
        this.evenementlidmaatschap = evenementlidmaatschap;
    }

    public double getJaarlidmaatschap() {
        return jaarlidmaatschap;
    }

    public void setJaarlidmaatschap(double jaarlidmaatschap) {
        this.jaarlidmaatschap = jaarlidmaatschap;
    }

    public Date getJaarpakket_uiterlijke_datum() {
        return jaarpakket_uiterlijke_datum;
    }

    public void setJaarpakket_uiterlijke_datum(Date jaarpakket_uiterlijke_datum) {
        this.jaarpakket_uiterlijke_datum = jaarpakket_uiterlijke_datum;
    }
}
