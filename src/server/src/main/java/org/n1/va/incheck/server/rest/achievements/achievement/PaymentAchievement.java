package org.n1.va.incheck.server.rest.achievements.achievement;

import java.io.Serializable;

/**
 */
public class PaymentAchievement implements Serializable {

    public static final long serialVersionUID = 99L;


    public AchievementAward performPayment(double amount, CentralAchievements central) {
        if (amount > central.highestPayment) {
            AchievementAward achievement = new AchievementAward("more-money-more-problems.jpg", "Achievement unlocked: \"Nieuw betaalrecord: " +
                    amount + " (vorige maximum was: " + central.highestPayment + ")\"");
            central.highestPayment = amount;
        }

        return null;
    }
}
