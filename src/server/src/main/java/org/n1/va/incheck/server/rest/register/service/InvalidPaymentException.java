package org.n1.va.incheck.server.rest.register.service;

/**
 * This class indicates that a payment was not valid.
 */
public class InvalidPaymentException extends RuntimeException {

    public InvalidPaymentException(String message) {
        super(message);
    }
}
