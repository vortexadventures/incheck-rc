package org.n1.va.incheck.server.rest.register.jsonmodel;

/**
 * Represents a mapping from a boeking to an event / purpose.
 */
public class PaymentHistoryMapping {

    public double amount;
    public String description;
    public String icon;
}
