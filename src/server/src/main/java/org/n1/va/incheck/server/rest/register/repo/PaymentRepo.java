package org.n1.va.incheck.server.rest.register.repo;

import org.n1.va.incheck.server.rest.register.model.*;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Repository
public class PaymentRepo extends AbstractRepo {

    @Autowired
    public PaymentRepo(DataSource dataSource) {
        super(dataSource);
    }

    public void insertPayment(ValeaBankBoeking boeking) {
        Map<String, Object> params = new HashMap<>();
        params.put("bedrag", boeking.bedrag);
        params.put("datum", boeking.datum);
        params.put("kassa", boeking.kassa);
        params.put("omschrijving1", boeking.omschrijving1);
        params.put("omschrijving2", boeking.omschrijving2);
        params.put("omschrijving3", boeking.omschrijving3);

        String sql = "INSERT INTO kasboeking (bedrag, datum, kassa, omschrijving1, omschrijving2, omschrijving3) " +
                " VALUES (:bedrag, :datum, :kassa, :omschrijving1, :omschrijving2, :omschrijving3)";
        int created = namedTemplate.update( sql, params);
        assertCreatedExactlyOneRow(created, sql);

        Long generatedId = template.queryForLong("SELECT LAST_INSERT_ID()");
        boeking.id = generatedId;
    }


    public void insertMapping(ValeaBoekingMapping mapping) {
        Map<String, Object> params = new HashMap<>();
        params.put("bedrag", mapping.bedrag);
        params.put("betaald_voor", mapping.betaald_voor);
        params.put("boeking_id", mapping.boekingId);
        params.put("event_id", mapping.eventId);
        params.put("omschrijving", mapping.omschrijving);
        params.put("product", mapping.product);

        String sql = "INSERT INTO boeking_event (boeking_id, event_id, bedrag, betaald_voor, omschrijving, product) " +
                "VALUES (:boeking_id, :event_id, :bedrag, :betaald_voor, :omschrijving, :product)";
        int created = namedTemplate.update( sql, params);
        assertCreatedExactlyOneRow(created, sql);
    }
}

