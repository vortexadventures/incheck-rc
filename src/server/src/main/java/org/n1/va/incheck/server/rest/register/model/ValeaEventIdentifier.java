package org.n1.va.incheck.server.rest.register.model;

/**
 * Defines an event in valea.
 */
public class ValeaEventIdentifier {

    public int jaar;
    public int eventName;

    public ValeaEventIdentifier(int jaar, int eventName) {
        this.jaar = jaar;
        this.eventName = eventName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValeaEventIdentifier that = (ValeaEventIdentifier) o;

        if (eventName != that.eventName) return false;
        if (jaar != that.jaar) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = jaar;
        result = 31 * result + eventName;
        return result;
    }
}
