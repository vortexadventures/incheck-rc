package org.n1.va.incheck.server.rest.extract;

import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ExtractRepo extends AbstractRepo {

    private List<ExtractRow> extractRows = null;

    private long lastQueryMillis = 0;




    private DataSource dataSource;

    @Autowired
    public ExtractRepo(DataSource dataSource) {
        super(dataSource);
        this.dataSource = dataSource;
    }

//    public List<ExtractRow> getExtract() {
//        Map<String, Object> params = new HashMap<String, Object>();
//        List<ExtractRow> rows= namedTemplate.query(
//                "SELECT plin, concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) name FROM deelnemers",
//                params,
//                ParameterizedBeanPropertyRowMapper.newInstance(ExtractRow.class)
//        );
//        return rows;
//    }

    public synchronized List<ExtractRow> getExtract() {
        if (System.currentTimeMillis() - lastQueryMillis < 1000 * 60 ) {
            return this.extractRows;
        }

        long start = System.currentTimeMillis();

        String sql = "SELECT plin, concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) name FROM deelnemers";

        final List<ExtractRow> extractRows = new ArrayList<>();

        template.queryWithRowCallback(sql, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet resultSet) throws SQLException {

                while (resultSet.next()) {

                    String name = resultSet.getString(2);
                    Object plin = resultSet.getObject(1);
                    if (plin != null) {
                        ExtractRow extract = new ExtractRow();
                        extract.setName(name);
                        extract.setPlin(plin.toString());
                        extractRows.add(extract);
                    }

                }
            }
        });

        long time = System.currentTimeMillis() - start;
        System.out.println("Query time: " + time + " ms.");

        this.extractRows = extractRows;
        lastQueryMillis = System.currentTimeMillis();
        return extractRows;
    }





}
