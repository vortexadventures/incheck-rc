package org.n1.va.incheck.server.rest.session;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This class manages the user session, specifically checking for user session time-outs
 */
@RequestMapping("/session")
@Controller
public class SessionController {

    public static final String SESSION_ATRIBUTE_LAST_ACTION = "lastAction";

    @ResponseBody
    @RequestMapping("isTimeout")
    public boolean isTimeOut(HttpServletRequest request) {
        HttpSession session = request.getSession();
        LocalDateTime lastAction = (LocalDateTime)session.getAttribute(SESSION_ATRIBUTE_LAST_ACTION);

        if (lastAction == null) {
            session.setAttribute(SESSION_ATRIBUTE_LAST_ACTION, LocalDateTime.now());
            return false;
        }

        LocalDateTime cutoffTime = lastAction.plusMinutes(5);
        LocalDateTime now = LocalDateTime.now();
        return (now.isAfter(cutoffTime));
    }
}
