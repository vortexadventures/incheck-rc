package org.n1.va.incheck.server.rest.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 * Base class for repos.
 */
public class AbstractRepo {

    public RedoLogNamedTemplate namedTemplate;
    public RedoLogTemplate template;

    public AbstractRepo(DataSource dataSource) {
        RedoLogger redoLogger = RedoLogger.INSTANCE;

        NamedParameterJdbcTemplate namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        namedTemplate = new RedoLogNamedTemplate(namedJdbcTemplate, redoLogger);

        JdbcTemplate jdbcTemplate = new JdbcTemplate (dataSource);
        template = new RedoLogTemplate(jdbcTemplate, redoLogger);
    }

    public void assertUpdatedExactlyOneRow(int updated, String sql) {
        assertCreatedExactlyOneRow(updated, sql);
    }

    public void assertCreatedExactlyOneRow(int created, String sql) {
        if (created != 1) {
            throw new RuntimeException("Failed to create/update exactly one row for the query: " + sql);
        }
    }

}
