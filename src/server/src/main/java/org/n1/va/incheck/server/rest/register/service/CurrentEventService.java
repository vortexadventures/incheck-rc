package org.n1.va.incheck.server.rest.register.service;

import org.n1.va.incheck.server.rest.register.model.*;
import org.n1.va.incheck.server.rest.register.repo.EventsRepo;
import org.n1.va.incheck.server.rest.register.repo.PrijzenRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class defines which event it is and allows for retrieval of this event's data.
 */
@Service
public class CurrentEventService {

    @Autowired
    private EventsRepo eventsRepo;

    @Autowired
    private PrijzenRepo prijzenRepo;

    public final ValeaEventIdentifier event = new ValeaEventIdentifier(2018, ValeaEventConst.MOOTS_1);

    public CurrentEventService() {
    }

    public int nextPlinForEvent(int plin) {
        List<Integer> plins = eventsRepo.getPlinsForEvents(this.event);

        int index = plins.indexOf(plin);
        return plins.get(index+1);
    }

    public String getEventName() {
        String name = deriveName();
        return name + " " + event.jaar;
    }

    private String deriveName() {
        switch(event.eventName) {
            case ValeaEventConst.SUMDAG: return "Sumdag";
            case ValeaEventConst.MOOTS_1: return "Moots-1";
            case ValeaEventConst.SUMMONING: return "Summoning";
            case ValeaEventConst.MOOTS_2: return "Moots-2";
            default: throw new Error("Event not recognized:" + event.eventName);
        }
    }

    public ValeaAllePrijzen getPrices() {
        ValeaEventPrijzen eventPrijzen = prijzenRepo.getPrijzen(event);

        eventPrijzen.initCrewPoortAndMonsterPrijs();

        ValeaJaarPrijzen jaarPrijzen = prijzenRepo.getJaarPrijzen(event.jaar);

        ValeaAllePrijzen prijzen = new ValeaAllePrijzen(eventPrijzen, jaarPrijzen );
        return prijzen;
    }

}
