package org.n1.va.incheck.server.rest.register.model;


import java.sql.Date;

/** This class represents a person as stored in Valea. */
public class ValeaDeelnemer {

    public long id;
    public String plin;
    public String name;
    public Date dateOfBirth;
    public boolean member;
    public String email;
    public String opmerking;
    public Date lid_start;
    public Date lid_eind;

}
