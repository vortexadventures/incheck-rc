package org.n1.va.incheck.server.rest.notes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest/JSON controller for Notes.
 */
@Controller
@RequestMapping("/notes")
public class NotesController {

    private Logger logger = LoggerFactory.getLogger(NotesController.class);

    @Autowired
    private NoteService noteService;

    @ResponseBody
	@RequestMapping(value = "get/{plin}", method = RequestMethod.GET)
	public List<JsonNoteLine> getNotes(@PathVariable("plin") int plin) {
        try {
            return noteService.getNotes(plin);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
	}

    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public List<JsonNoteLine> pay(@RequestBody JsonNoteInput noteInput) {
        try {
            return noteService.addNote(noteInput);
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}