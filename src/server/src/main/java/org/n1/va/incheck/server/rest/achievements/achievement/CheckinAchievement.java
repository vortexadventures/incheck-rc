package org.n1.va.incheck.server.rest.achievements.achievement;

import java.io.Serializable;

/**
 */
public class CheckinAchievement implements Serializable {

    public static final long serialVersionUID = 99L;

    private String user;

    public int checkinCount = 0;

    public AchievementAward performCheckin(int plin, CentralAchievements centralAchievements) {


        if (plin == 2524) {
            return new AchievementAward("achievements-2/nicolas_cage_can_become_just_anyone_640_51.jpg", "Achievement unlocked: \"Heh Emile, je zei dat je hiermee ging stoppen...\"");
        }

        if (plin == 1085 || plin == 1490) {
            return new AchievementAward("achievements-2/va-plotufo.png", "Achievement unlocked: \"Toedeloe kutnoobs!\"");
        }

        if (plin == 1286) {
            return new AchievementAward("achievements-2/Toiletheroes.jpg", "Achievement unlocked: \"Deze gast regelt je toilethelden...\"");
        }

        centralAchievements.totalCheckins ++;

        if ( centralAchievements.totalCheckins == 100) {
            return new AchievementAward("achievements-2/100.png", "Achievement unlocked: \"Gefeliciteerd, de honderste klant!\"");
        }

        if ( centralAchievements.totalCheckins == 200) {
            return new AchievementAward("achievements-2/200.png", "Achievement unlocked: \"Jaa, de tweehonderste klant alweer...\"");
        }

        if ( centralAchievements.totalCheckins == 300) {
            return new AchievementAward("achievements-2/300.jpg", "Achievement unlocked: \"THIS IS SPARTA! (en ook klant nr 300)!\"");
        }

        if ( centralAchievements.totalCheckins == 350) {
            return new AchievementAward("achievements-2/350.jpg", "Achievement unlocked: \"Klant nr 350, op naar de 400!\"");
        }

        if ( centralAchievements.totalCheckins == 400) {
            return new AchievementAward("achievements-2/400.jpg", "Achievement unlocked: \"Yes, we hebben het gehaald, 400 deelnemers. Net als vroegah.\"");
        }

        if ( centralAchievements.totalCheckins == 450) {
            return new AchievementAward("achievements-2/450.jpg", "Achievement unlocked: \"Op naar de 500...\"");
        }

        if ( centralAchievements.totalCheckins == 500) {
            return new AchievementAward("achievements-2/500.jpg", "Achievement unlocked: \"De 500ste klant! Geef die persoon een gratis jaarpakket, " +
                    "maak hem/haar erelid en doe daar ook nog eens een t-shirt in iedere maat bij! 500 deelnemers... wohoo!\"");
        }


        return null;
    }
}
