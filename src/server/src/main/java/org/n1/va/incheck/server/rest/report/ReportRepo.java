package org.n1.va.incheck.server.rest.report;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.extract.ExtractRow;
import org.n1.va.incheck.server.rest.register.model.ValeaEventIdentifier;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repo for occupancy report.
 */
@Repository
public class ReportRepo extends AbstractRepo {

    @Autowired
    public ReportRepo(DataSource dataSource) {
        super(dataSource);
    }

    public List<ReportRow> getOccupants(Date date, ValeaEventIdentifier event) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("datum", date);
        params.put("jaar", event.jaar);
        params.put("eventName", event.eventName);

        List<ReportRow> rows= namedTemplate.query(
                "SELECT d.plin, concat(trim(concat_ws(' ', d.voornaam, d.tussenvoegsels)), ' ', d.achternaam) name " +
                        " FROM deelnemers d, events e " +
                        " WHERE e.deelnemer_id = d.id " +
                        " AND e.jaar = :jaar " +
                        " AND e.naam = :eventName " +
                        " AND date(e.incheck_date) <= date(:datum) " +
                        " AND (e.uitcheck_date is null or date(e.uitcheck_date) > date(:datum)) " +
                        " ORDER BY d.plin",
                params,
                ParameterizedBeanPropertyRowMapper.newInstance(ReportRow.class)
        );
        return rows;
    }

    public List<MutationRow> getCashRegisterMutations(Date startDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("startDate", startDate);

        List<MutationRow> rows = namedTemplate.query(
                "SELECT kassa, bedrag, omschrijving, uitgevoerd_door gebruiker, tijdstip " +
                        " FROM kasmutatie" +
                        " WHERE tijdstip >= :startDate" +
                        " ORDER BY tijdstip",
                params,
                ParameterizedBeanPropertyRowMapper.newInstance(MutationRow.class)
        );

        return rows;
    }
}
