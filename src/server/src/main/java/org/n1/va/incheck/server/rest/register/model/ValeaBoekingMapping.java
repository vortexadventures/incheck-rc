package org.n1.va.incheck.server.rest.register.model;

/**
 * A mapping from a boeking to an event
 */
public class ValeaBoekingMapping {

    public long eventId;
    public long boekingId;
    public double bedrag;
    public String betaald_voor;
    public String product;
    public String omschrijving;

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getBoekingId() {
        return boekingId;
    }

    public void setBoekingId(long boekingId) {
        this.boekingId = boekingId;
    }

    public double getBedrag() {
        return bedrag;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    public String getBetaald_voor() {
        return betaald_voor;
    }

    public void setBetaald_voor(String betaald_voor) {
        this.betaald_voor = betaald_voor;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }
}
