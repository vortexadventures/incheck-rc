package org.n1.va.incheck.server.rest.event;

import org.n1.va.incheck.server.rest.register.model.ValeaAllePrijzen;
import org.n1.va.incheck.server.rest.register.service.CurrentEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This class defines REST calls relating to getting event data.
 */
@RequestMapping("event")
@Controller
public class EventController {

    @Autowired
    private CurrentEventService currentEventService;

    @ResponseBody
    @RequestMapping("name")
    public String name() {
        String name = currentEventService.getEventName();
        return name;
    }

    @ResponseBody
    @RequestMapping("prices")
    public ValeaAllePrijzen prices() {
        ValeaAllePrijzen prices = currentEventService.getPrices();





        return prices;
    }

}
