package org.n1.va.incheck.server.rest.util;

/**
 * Util methods for handling whitespace
 */
public class SpaceUtil {

    public static String fourLeadingSpaces(String input) {
        if (input == null ) {
            return "    ";
        }
        StringBuilder builder = new StringBuilder();
        if ( input.length() < 1) {
            builder.append(' ');
        }
        if ( input.length() < 2) {
            builder.append(' ');
        }
        if ( input.length() < 3) {
            builder.append(' ');
        }
        if ( input.length() < 4) {
            builder.append(' ');
        }
        builder.append(input);
        return builder.toString();
    }
}
