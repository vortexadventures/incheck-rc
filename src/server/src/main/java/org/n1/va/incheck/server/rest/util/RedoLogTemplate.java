package org.n1.va.incheck.server.rest.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import java.util.List;
import java.util.Map;

/**
 * This is a wrapper around a Jdbc templat that logs the queries to file as a sort of redo log.
 */
public class RedoLogTemplate {

    private JdbcTemplate template;
    private RedoLogger redoLogger;

    protected RedoLogTemplate(JdbcTemplate template, RedoLogger redoLogger) {
        super();
        this.template = template;
        this.redoLogger = redoLogger;
    }

    public int update(java.lang.String sql, java.lang.Object... args)  {
        redoLogger.redoLog(sql, args);
        return template.update(sql, args);
    }

    public <T> List<T> query(String sql, ParameterizedRowMapper<T> mapper, Object... args) {
        return template.query(sql, mapper, args);
    }

    public <T> T queryForObject(String sql, ParameterizedRowMapper<T> mapper, Object... args) {
        return template.queryForObject(sql, mapper, args);
    }

    public Long queryForLong(String sql) {
        return template.queryForLong(sql);
    }

    public <T> T queryForObject(String sql, Class<T> clazz, Object... args) {
        return template.queryForObject(sql, clazz, args);
    }

    public List<Map<String, Object>> queryForList(String sql) {
        return template.queryForList(sql);
    }

    public void queryWithRowCallback(String sql, RowCallbackHandler callbackHandler) {
        template.query(sql, callbackHandler);
    }

}
