package org.n1.va.incheck.server.rest.session;

import org.joda.time.LocalDateTime;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This filter sets the attribute (SessionController.SESSION_ATRIBUTE_LAST_ACTION) to the current time
 * to indicate that an action was performed by the client, for all actions apart from the actions on the "/session" url
 * which is managed by the SessionController. This latter is to prevent checks on session time-out to trigger a renewal of the time-out.
 */
public class SessionTimeOutFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest)servletRequest;
            parseRequest(request);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void parseRequest(HttpServletRequest request) {
        String path = request.getServletPath();
        if ( path.startsWith("/session")) {
            return;
        }

        LocalDateTime now = LocalDateTime.now();
        HttpSession session = request.getSession();
        session.setAttribute(SessionController.SESSION_ATRIBUTE_LAST_ACTION, now);
    }

    @Override
    public void destroy() {
    }
}
