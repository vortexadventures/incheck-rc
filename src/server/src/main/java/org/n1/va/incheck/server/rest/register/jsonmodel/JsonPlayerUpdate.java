package org.n1.va.incheck.server.rest.register.jsonmodel;

/**
 * Input for player update
 */
public class JsonPlayerUpdate {
    public int plin;
    public String input;

    public int getPlin() {
        return plin;
    }

    public void setPlin(int plin) {
        this.plin = plin;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
