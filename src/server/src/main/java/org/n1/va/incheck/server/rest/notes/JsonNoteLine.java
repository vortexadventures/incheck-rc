package org.n1.va.incheck.server.rest.notes;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Front end version of Note.
 */
public class JsonNoteLine {
    public String text;
    public String user;
    public String time;

    public JsonNoteLine(Note note) {
        this.text = note.tekst;
        this.user = note.gebruiker;

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        this.time = format.format(note.tijdstip);
    }

    public JsonNoteLine() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
