package org.n1.va.incheck.server.rest.register.jsonmodel;

/**
 * An item bought by the contestant.
 */
public class PaymentItem {

    public int total;
    public String purpose;
    public String role;


    // properties to ignore
    public String name;
    public String type;

    // ---------------------------------

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
