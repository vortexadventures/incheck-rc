package org.n1.va.incheck.server.rest.register.jsonmodel;


/**
 * Status on what a person has paid / must pay.
 */
public class RegisterPaymentInfo {

    public String event;
    public String membership;

    public RegisterPaymentInfo() {
    }

    public RegisterPaymentInfo(String event, String membership) {
        this.event = event;
        this.membership = membership;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }
}
