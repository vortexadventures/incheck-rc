package org.n1.va.incheck.server.rest.register.service;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.jsonmodel.*;
import org.n1.va.incheck.server.rest.register.model.*;
import org.n1.va.incheck.server.rest.register.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines the service that supports retrieving payment histories.
 */
@Service
public class PaymentHistoryService {

    Logger logger = LoggerFactory.getLogger(PaymentHistoryService.class);

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private DeelnemerRepo deelnemerRepo;

    @Autowired
    private PaymentHistoryRepo paymentHistoryRepo;

    @Autowired
    private EventsRepo eventsRepo;

    @Autowired
    private PrijzenRepo prijzenRepo;

    public List<PaymentHistoryBoeking> getHistory(int plin) {
        List<PaymentHistoryBoeking> jsonBoekingen = new ArrayList<>();
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(plin);

        List<ValeaBankBoeking> boekingen  = paymentHistoryRepo.getHistory(deelnemer);

        for (ValeaBankBoeking boeking: boekingen) {
            PaymentHistoryBoeking jsonBoeking = createJsonBoeking(boeking);
            jsonBoekingen.add(jsonBoeking);

            List<ValeaBoekingMapping> mappings = paymentHistoryRepo.getMappings(boeking);
            for (ValeaBoekingMapping mapping: mappings) {
                ValeaEvent event = eventsRepo.getEvent(mapping.eventId);
                String eventNaam, rol;
                if (event != null ) {
                    if ( event.deelnemer_id != deelnemer.id) {
                        continue;
                    }
                    eventNaam = ValeaEventConst.getEventNaam(event.naam) + "-" + event.jaar;
                    rol = event.describeRole();
                }
                else {
                    eventNaam = "";
                    rol = "onbekend";
                }

                if (ValeaEventConst.TOEGANG.equals(mapping.betaald_voor) ) {

                    boolean jaarpakket = false;
                    if (boeking.bedrag >= prijzenRepo.getJaarPrijzen(event.jaar).crew_jaarpakket) {
                        int mappingsForThisPlayer = 0;
                        for (ValeaBoekingMapping m: mappings) {
                            ValeaEvent e = eventsRepo.getEvent(m.eventId);
                            if (e != null && e.deelnemer_id == deelnemer.id){
                                mappingsForThisPlayer++;
                            }
                        }
                        if ( mappingsForThisPlayer >= 3) {
                            jaarpakket = true;
                        }
                    }

                    handleToegangMapping(mapping, jsonBoeking, event, eventNaam, rol, boeking, jaarpakket, deelnemer);
                }
                else {
                    handleNonToegangMapping(mapping, jsonBoeking, eventNaam);
                }
            }
        }
        return jsonBoekingen;
    }

    private void handleNonToegangMapping(ValeaBoekingMapping mapping, PaymentHistoryBoeking jsonBoeking, String eventNaam) {
        PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping);

        if ( mapping.omschrijving != null && !mapping.omschrijving.isEmpty()) {
            jsonMapping.description = mapping.omschrijving + " " + eventNaam;
        }
        else {
            jsonMapping.description = mapping.betaald_voor + " " + eventNaam;
        }
        jsonMapping.icon = "icon-minus";
        jsonBoeking.mappings.add(jsonMapping);
    }

    private void handleToegangMapping(ValeaBoekingMapping mapping, PaymentHistoryBoeking jsonBoeking,
                                      ValeaEvent event, String eventNaam, String rol, ValeaBankBoeking boeking,
                                      boolean jaarpakket, ValeaDeelnemer deelnemer) {
        if (event == null ) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping);
            jsonMapping.description = "boekingsfout, onbekend-event";
            jsonMapping.icon = "icon-exclamation-sign";
            jsonBoeking.mappings.add(jsonMapping);
            return;
        }

        if (event.naam == ValeaEventConst.VERENIGINGSLIDMAATSCHAP) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping);
            if (deelnemer.member) {
                jsonMapping.description = "Verenigingslidmaatschap " + event.jaar;
            }
            else {
                jsonMapping.description = "3x Evenementslidmaatschap " + event.jaar;
            }
            jsonMapping.icon = "icon-leaf";
            jsonBoeking.mappings.add(jsonMapping);
            return;
        }

        if (event.speciaal_bedrag) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping);
            String omschrijving = (mapping.omschrijving == null) ? "Speciaalbedrag" : mapping.omschrijving;
            jsonMapping.description = omschrijving + " " + eventNaam;
            jsonMapping.icon = "icon-asterisk";
            jsonBoeking.mappings.add(jsonMapping);
            return;
        }


        double amountLeft = mapping.bedrag;

        ValeaEventPrijzen prijzen = prijzenRepo.getPrijzen(event);
        String roleCode = event.getRoleCode();
        double eventPrice;
        if (jaarpakket) {
            if (ValeaEventConst.ROLE_CREW.equals(roleCode) || ValeaEventConst.ROLE_MONSTER.equals(roleCode)) {
                eventPrice = prijzen.getPrijs_jaarpakket_crew();
            }
            else {
                eventPrice = prijzen.getPrijs_jaarpakket();
            }
        }
        else {
            LocalDate paymentDate = new LocalDate(boeking.datum);
            boolean voorInschrijving = prijzen.inTimeForVoorInschrijving(paymentDate);
            eventPrice = prijzen.priceForEvent(event, voorInschrijving);
        }

        if (amountLeft >= eventPrice) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping, eventPrice);
            jsonMapping.description = rol + " " + eventNaam;
            if (ValeaEventConst.ROLE_CREW.equals(roleCode) || ValeaEventConst.ROLE_MONSTER.equals(roleCode)) {
                jsonMapping.icon = "icon-bookmark";
            }
            else {
                jsonMapping.icon = "icon-user";
            }
            jsonBoeking.mappings.add(jsonMapping);
            amountLeft -= eventPrice;
        }

        double evenementlidmaatschap = prijzenRepo.getJaarPrijzen(event.jaar).evenementlidmaatschap;
        double jaarlidmaatschap = prijzenRepo.getJaarPrijzen(event.jaar).jaarlidmaatschap;
        if (amountLeft >=  jaarlidmaatschap && deelnemer.member) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping, jaarlidmaatschap);
            jsonMapping.description = "Verenigingslidmaatschap " + event.jaar;
            jsonMapping.icon = "icon-leaf";
            jsonBoeking.mappings.add(jsonMapping);
            amountLeft -= jaarlidmaatschap;
        }

        if (amountLeft >=  evenementlidmaatschap) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping, evenementlidmaatschap);
            jsonMapping.description = "Evenementslidmaatschap " + eventNaam;
            jsonMapping.icon = "icon-leaf";
            jsonBoeking.mappings.add(jsonMapping);
            amountLeft -= evenementlidmaatschap;
        }

        if (amountLeft >= 0.01d ) {
            PaymentHistoryMapping jsonMapping = createStandardJsonMappingForPaymentMapping(mapping, amountLeft);
            jsonMapping.description = "(Over: " +  amountLeft + ")";
            jsonMapping.icon = "icon-question-sign";
            jsonBoeking.mappings.add(jsonMapping);
        }
    }

    private PaymentHistoryMapping createStandardJsonMappingForPaymentMapping(ValeaBoekingMapping mapping) {
        return createStandardJsonMappingForPaymentMapping(mapping, mapping.bedrag);
    }

    private PaymentHistoryMapping createStandardJsonMappingForPaymentMapping(ValeaBoekingMapping mapping, double price) {
        PaymentHistoryMapping jsonMapping = new PaymentHistoryMapping();
        jsonMapping.amount = price * 100;
        return jsonMapping;
    }


    private PaymentHistoryBoeking createJsonBoeking(ValeaBankBoeking boeking) {
        PaymentHistoryBoeking jsonBoeking = new PaymentHistoryBoeking();
        jsonBoeking.amount = boeking.bedrag * 100;
        jsonBoeking.date = boeking.datum.getTime();
        jsonBoeking.description = boeking.getOmschrijving1();
        jsonBoeking.payer = boeking.naam_tegenrekening;
        jsonBoeking.mappings = new ArrayList<>();

        return jsonBoeking;
    }
}
