package org.n1.va.incheck.server.rest.cashregister.model;

import java.util.Date;

/**
 * A cash register mutation as used sent/received in json format.
 */
public class JsonCashMutation {

    public String user;
    public String showDate;
    public Date date;
    public String reason;
    public double amount;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
