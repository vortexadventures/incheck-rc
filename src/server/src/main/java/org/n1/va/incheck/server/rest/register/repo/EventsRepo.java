package org.n1.va.incheck.server.rest.register.repo;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.register.model.ValeaEvent;
import org.n1.va.incheck.server.rest.register.model.ValeaEventIdentifier;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EventsRepo extends AbstractRepo {

    @Autowired
    public EventsRepo(DataSource dataSource) {
        super(dataSource);
    }
    public ValeaEvent getEventDataForPerson(ValeaDeelnemer deelnemer, int jaar, int eventName) {
        return getEventDataForPerson(deelnemer.id, jaar, eventName);
    }

    private ValeaEvent getEventDataForPerson(long deelnemerId, int jaar, int eventName) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("deelnemer_id", deelnemerId);
        params.put("jaar", jaar);
        params.put("eventNaam", eventName);

        List<ValeaEvent> events = namedTemplate.query("SELECT" +
                " id, deelnemer_id, crew, monster, ehbo, barlid, bestuur, inschrijvingsformulier, " +
                " evenementlidmaatschap_betaald, toegang_betaald, k12, k16, speciaal_bedrag, " +
                " tafels, banken, jaar, naam, incheck_date, uitcheck_date" +
                " FROM events" +
                " WHERE deelnemer_id = :deelnemer_id " +
                " AND jaar = :jaar" +
                " AND naam = :eventNaam", params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaEvent.class)
        );
        if ( events.isEmpty()) {
            return null;
        }
        if (events.size()> 1) {
            throw new RuntimeException("Found more than one event record for player.id=" + deelnemerId);
        }
        return events.get(0);
    }


    public List<Long> getDeelnemersForEvent(int jaar, int eventName) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jaar", jaar);
        params.put("eventName", eventName);

        return namedTemplate.query( "select events.deelnemer_id from events " +
                "where events.jaar = :jaar and events.naam = :eventName",
                params, ParameterizedBeanPropertyRowMapper.newInstance(Long.class));
    }

    public void updateCheckin(ValeaEvent event, Date now) {
        String sql = "UPDATE events SET incheck_date=? WHERE id=?";
        int updated = template.update(sql, now, event.id);
        assertUpdatedExactlyOneRow(updated, sql);
    }


    public void updateCheckout(ValeaEvent event, Date now) {
        String sql = "UPDATE events SET uitcheck_date=? WHERE id=?";
        int updated = template.update(sql, now, event.id);
        assertUpdatedExactlyOneRow(updated, sql);
    }

    public void updateEventPartial(ValeaEvent event) {
        Map<String, Object> params = createEventParams(event);
        String sql = "UPDATE events SET " +
                " crew = :crew, monster = :monster, ehbo = :ehbo, barlid = :barlid, bestuur = :bestuur, " +
                " k12 = :k12, k16 = :k16, speciaal_bedrag = :speciaal_bedrag, " +
                "evenementlidmaatschap_betaald = :evenementlidmaatschap_betaald" +
                " WHERE id = :id ";

        int updated = namedTemplate.update(sql, params);
        assertUpdatedExactlyOneRow(updated, sql);
    }

    private Map<String, Object> createEventParams(ValeaEvent event) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", event.id);
        params.put("crew", event.crew);
        params.put("monster", event.monster);
        params.put("ehbo", event.ehbo);
        params.put("barlid", event.barlid);
        params.put("bestuur", event.bestuur);
        params.put("k12", event.k12);
        params.put("k16", event.k16);
        params.put("speciaal_bedrag", event.speciaal_bedrag);
        params.put("jaar", event.jaar);
        params.put("naam", event.naam);
        params.put("deelnemer_id", event.deelnemer_id);
        params.put("inschrijvingsformulier", event.inschrijvingsformulier);
        params.put("evenementlidmaatschap_betaald", event.evenementlidmaatschap_betaald);
        params.put("toegang_betaald", event.toegang_betaald);
        params.put("toezichthouder", "");
        params.put("tafels", event.tafels);
        params.put("banken", event.banken);

        return params;
    }

    public void createEvent(ValeaEvent event) {
        Map<String, Object> params = createEventParams(event);
        String sql = "INSERT INTO events " +
                "(jaar, deelnemer_id, naam, crew, ehbo, barlid, bestuur, monster, k12, k16, tafels, banken, " +
                "speciaal_bedrag, " +
                "inschrijvingsformulier, evenementlidmaatschap_betaald, toegang_betaald, toezichthouder )" +
                " VALUES" +
                "(:jaar, :deelnemer_id, :naam, :crew, :ehbo, :barlid, :bestuur, :monster, " +
                " :k12, :k16, :tafels, :banken, :speciaal_bedrag, " +
                " :inschrijvingsformulier, :evenementlidmaatschap_betaald, :toegang_betaald, :toezichthouder)";

        int created = namedTemplate.update(sql, params);
        assertCreatedExactlyOneRow(created, sql);

        Long generatedId = template.queryForLong("SELECT LAST_INSERT_ID()");
        event.id = generatedId;
    }

    public ValeaEvent getEvent(long eventId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", eventId);

        ValeaEvent event = namedTemplate.queryForObject("SELECT" +
                " id, deelnemer_id, crew, monster, ehbo, barlid, bestuur, inschrijvingsformulier, " +
                " evenementlidmaatschap_betaald, toegang_betaald, k12, k16, speciaal_bedrag, " +
                " tafels, banken, jaar, naam, incheck_date, uitcheck_date" +
                " FROM events" +
                " WHERE id = :id ", params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaEvent.class)
        );
        return event;
    }

    public List<Integer> getPlinsForEvents(ValeaEventIdentifier event) {
        String sql = "select deelnemers.plin plin FROM deelnemers, events " +
                " WHERE events.jaar=? AND events.naam=? and events.deelnemer_id = deelnemers.id";

        return template.query(sql, new ParameterizedRowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int row) throws SQLException {
                return rs.getInt("plin");
            }
        }, event.jaar, event.eventName);
    }
}
