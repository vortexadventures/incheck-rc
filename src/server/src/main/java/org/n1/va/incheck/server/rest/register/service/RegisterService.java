package org.n1.va.incheck.server.rest.register.service;

import org.joda.time.LocalDate;
import org.n1.va.incheck.server.rest.register.jsonmodel.RegisterInfo;
import org.n1.va.incheck.server.rest.register.jsonmodel.RegisterPaymentBooleans;
import org.n1.va.incheck.server.rest.register.jsonmodel.RegisterPaymentInfo;
import org.n1.va.incheck.server.rest.register.model.*;
import org.n1.va.incheck.server.rest.register.repo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class defines the service that supports registration actions (calculatin what is owed, and registering).
 */
@Service
public class RegisterService {

    Logger logger = LoggerFactory.getLogger(RegisterService.class);

    @Autowired
    private CurrentEventService currentEventService;

    @Autowired
    private DeelnemerRepo deelnemerRepo;

    @Autowired
    private PaidForEventRepo paidRepo;

    @Autowired
    private EventsRepo eventsRepo;

    @Autowired
    private PrijzenRepo prijzenRepo;


    public RegisterInfo getRegistrationInfo(int plin) {
        ValeaDeelnemer deelnemer = deelnemerRepo.getDeelnemer(plin);

        ValeaEvent event = eventsRepo.getEventDataForPerson(deelnemer, currentEventService.event.jaar,  currentEventService.event.eventName);
        if (event == null) {
            return createEmptyEventInfo(deelnemer);
        }

        RegisterInfo info = parseEventPayment(deelnemer, event);

        return info;
    }

    private RegisterInfo parseEventPayment(ValeaDeelnemer deelnemer, ValeaEvent event) {
        RegisterInfo info = new RegisterInfo(deelnemer);
        info.roleCode = event.getRoleCode();
        info.roleDescription = event.describeRole();
        info.checkedIn = parseCheckedIn(event);

        if (event.roleEntersForFree()) {
            parseFreeRole(info);
            return info;
        }

        Double amountToPayForClubMembership = determineAmountToPayForClubMembership(deelnemer, event.jaar);

        boolean jaarpakket = paidRepo.determineIfPaidByJaarPakket(event);
        if (jaarpakket) {
            parseJaarPakket(deelnemer, event, amountToPayForClubMembership, info);
        }
        else {
            parseEventDeelnemer(deelnemer, event, amountToPayForClubMembership, info);
        }
        return info;
    }

    private boolean parseCheckedIn(ValeaEvent event) {
        if (event.incheck_date == null) {
            return false;
        }
        if (event.uitcheck_date != null) {
            return false;
        }
        return true;
    }

    private Double determineAmountToPayForClubMembership(ValeaDeelnemer deelnemer, int jaar) {
        ValeaEvent lidmaatschapsEvent = eventsRepo.getEventDataForPerson(deelnemer, jaar, ValeaEventConst.VERENIGINGSLIDMAATSCHAP);

        if (lidmaatschapsEvent == null) {
            return null;
        }

        double totalPaidForMembership = paidRepo.getPaidForEvent(lidmaatschapsEvent);
        double totalRequired = prijzenRepo.getJaarPrijzen(jaar).jaarlidmaatschap;

        double totalToPay = totalRequired - totalPaidForMembership;
        return totalToPay;
    }

    private void parseEventDeelnemer(ValeaDeelnemer deelnemer, ValeaEvent event, Double amountToPayForClubMembership, RegisterInfo info) {
        double eventMembershipFee = 0d;
        if (amountToPayForClubMembership != null ) {
            if ( amountToPayForClubMembership == 0.0d) {
                info.paid.membership = (deelnemer.member)? "Verenigingslid" : "Evenementslid";
                info.paidInFull.membership = true;
            }
            else {
                info.paidInFull.membership = false;
                info.paid.membership = (deelnemer.member)? "Verenigingslid" : "Evenementslid";
                info.paid.membership += ", mist: " + amountToPayForClubMembership;
            }
        }
        else {
            eventMembershipFee = prijzenRepo.getJaarPrijzen(event.jaar).evenementlidmaatschap;
        }

        double eventAmountPaid = paidRepo.getPaidForEvent(event);
        ValeaEventPrijzen prijzen = prijzenRepo.getPrijzen(currentEventService.event);

        if (event.speciaal_bedrag) {
            handleSpeciaalBedrag(event, amountToPayForClubMembership, eventAmountPaid, info, eventMembershipFee);
            return;
        }



        LocalDate paymentDate = paidRepo.getPaymentDate(event);
        boolean voorInschrijving = prijzen.inTimeForVoorInschrijving(paymentDate);
        double eventPrice = prijzen.priceForEvent(event, voorInschrijving);

        String paidEvent = voorInschrijving ? "Voorinschrijving" : "Poortbetaling";
        double mustPayEvent = determineAmountToPay(eventAmountPaid, eventPrice);

        double mustPayTotal = mustPayEvent + eventMembershipFee;

        if (mustPayTotal <= 0 && info.paidInFull.membership == false) {
            // geen verenigingslid
            info.paid.membership = "Evenementslid";
            info.paidInFull.membership = true;
        }

        if (mustPayTotal == 0) {
            info.paid.event = paidEvent;
            info.paidInFull.event = true;
            return;
        }

        if ( mustPayTotal == eventMembershipFee && info.paidInFull.membership == false) {
            info.paid.event = paidEvent;
            info.paidInFull.event = true;

            info.paid.membership = "Ontbreekt";
            info.paidInFull.membership = false;
            return;
        }
        if (mustPayTotal < 0 ) {
            info.paid.event = "Te veel: " + (0-(mustPayTotal));
            info.paidInFull.event = true;
            return;
        }
        info.paid.event = "Onvolledig, mist: " + (mustPayTotal );
        info.paidInFull.event = false;

        if (info.paidInFull.membership == false) {
            // geen verenigingslids
            info.paid.membership = "Ontbreekt";
            info.paidInFull.membership = false;
        }
    }

    private void handleSpeciaalBedrag(ValeaEvent event, Double amountToPayForClubMembership, double eventAmountPaid, RegisterInfo info, double eventMembershipFee) {
        if (amountToPayForClubMembership == null) {
            info.paid.membership = "(Controleer handmatig)";
            info.paidInFull.membership = false;
        }
        info.paid.event = "(Controleer handmatig)";
        info.paidInFull.event = false;

    }

    private void parseFreeRole(RegisterInfo info) {
        info.paid = new RegisterPaymentInfo("n.v.t.", "n.v.t.");
        info.paidInFull = new RegisterPaymentBooleans(true, true);
    }

    private double determineAmountToPay(Double eventAmountPaid, double eventPrice) {
        if (eventAmountPaid != null) {
            return eventPrice - eventAmountPaid;
        }
        return eventPrice;
    }

    private RegisterInfo createEmptyEventInfo(ValeaDeelnemer deelnemer) {
        RegisterInfo info = new RegisterInfo(deelnemer);
        info.roleCode = null;
        info.paid = new RegisterPaymentInfo("-", "-");
        return info;
    }

    private void parseJaarPakket(ValeaDeelnemer deelnemer, ValeaEvent event, Double amountToPayForClubMembership, RegisterInfo info) {
        if (event.speciaal_bedrag) {
            parseJaarPakketSpeciaalBedrag(deelnemer, event, amountToPayForClubMembership, info);
            return;
        }

        double paidForClubMembership = 0;
        if (amountToPayForClubMembership != null) {
            paidForClubMembership = prijzenRepo.getJaarPrijzen(event.jaar).jaarlidmaatschap - amountToPayForClubMembership;
        }

        double totalPaidThisYear = paidRepo.findTotalAmountPaidForThisYear(deelnemer, currentEventService.event.jaar);
        double requiredForEvents = determineJaarpakketRequired(event);
        double requiredForMembership = parseRequiredForMembershipJaarPakket(deelnemer, amountToPayForClubMembership, event.jaar, info);
        double mustPay = (requiredForEvents + requiredForMembership) - (totalPaidThisYear - paidForClubMembership);

        if ( mustPay == 0) {
            info.paid.event = "Jaarpakket";
            info.paidInFull.event = true;
            info.paidInFull.membership = true;
            info.paid.membership = (deelnemer.member) ? "Verenigingslid" : "Evenementslid";
            return;
        }
        if ( mustPay < 0) {
            info.paid.event = "Jaarpakket, te veel:" + (0- mustPay);
            info.paidInFull.event = true;
            info.paidInFull.membership = true;
            info.paid.membership = (deelnemer.member) ? "Verenigingslid" : "Evenementslid";
            return;
        }
        double jaarlidmaatschap = prijzenRepo.getJaarPrijzen(event.jaar).evenementlidmaatschap * 3;
        double verenigingslidmaatschap = prijzenRepo.getJaarPrijzen(event.jaar).jaarlidmaatschap;

        double lidmaatschap = (amountToPayForClubMembership == null) ? jaarlidmaatschap : verenigingslidmaatschap;

        if (mustPay < lidmaatschap) {
            info.paid.event = "Jaarpakket";
            info.paidInFull.event = true;
            info.paidInFull.membership = false;
            info.paid.membership = (deelnemer.member) ? "Verenigingslid" : "Evenementslid";
            info.paid.membership += " mist: " + mustPay;
        }
        else {
            double eventMissing = mustPay - lidmaatschap;
            info.paid.event = "Jaarpakket, mist: " + eventMissing;
            info.paidInFull.event = true;
            info.paidInFull.membership = false;
            info.paid.membership = "Ontbreekt";
            info.paid.membership += " mist: " + lidmaatschap;
        }
    }

    private void parseJaarPakketSpeciaalBedrag(ValeaDeelnemer deelnemer, ValeaEvent event, Double amountToPayForClubMembership, RegisterInfo info) {
        info.paid.event = "(Controleer jaarpakket)";
        info.paidInFull.event = false;
        info.paidInFull.membership = false;
        info.paid.membership = "(Controleer jaarpakket)";
    }

    private double parseRequiredForMembershipJaarPakket(ValeaDeelnemer deelnemer, Double amountToPayForClubMembership, int jaar, RegisterInfo info) {
        if (amountToPayForClubMembership == null) {
            double toPay = prijzenRepo.getJaarPrijzen(jaar).evenementlidmaatschap * 3;
            return toPay;
        }
        if ( amountToPayForClubMembership == 0.0d) {
            info.paid.membership = "Betaald";
        }
        else {
            info.paidInFull.membership = false;
            info.paid.membership = "Onvolledig, mist: " + amountToPayForClubMembership;
        }
        return amountToPayForClubMembership;
    }

    private double determineJaarpakketRequired(ValeaEvent event) {
        ValeaJaarPrijzen prijzen = prijzenRepo.getJaarPrijzen(event.jaar);
        if (event.roleIsSpeler()) {
            return prijzen.jaarpakket;
        }
        if (event.speciaal_bedrag) {
            throw new RuntimeException("Speciaal jaarpakket heeft geen specifieke prijs, los anders op.");
            // gokje: erelid
        }
        return prijzen.crew_jaarpakket;
    }
}
