package org.n1.va.incheck.server.rest.achievements.achievement;

import java.io.Serializable;

/**
 * This class maintains shared achievement data.
 */
public class CentralAchievements implements Serializable {

    public static final long serialVersionUID = 99L;

    public int totalCheckins = 0;

    public double totalMoneyCheckedOut = 0d;

    public double highestPayment = 150.0d;

}
