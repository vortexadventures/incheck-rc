package org.n1.va.incheck.server.rest.achievements;

import org.n1.va.incheck.server.rest.achievements.achievement.AchievementAward;
import org.n1.va.incheck.server.rest.achievements.achievement.CentralAchievements;
import org.n1.va.incheck.server.rest.achievements.achievement.CheckinAchievement;
import org.n1.va.incheck.server.rest.achievements.achievement.PaymentAchievement;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 *Service voor achievements
 */
@Service
public class AchievementService {

    public static final String CHECKIN_ACHIEVEMENTS_LOCATION = "checkinAchievements.ser";
    public static final String PAYMENT_ACHIEVEMENTS_LOCATION = "paymentAchievements.ser";
    public static final String CENTRAL_ACHIEVEMENTS_LOCATION = "centralAchievements.ser";
    private Map<String, AchievementAward> receivedAchievements = new HashMap<>();

    private Map<String, CheckinAchievement> checkins;
    private Map<String, PaymentAchievement> payments;
    private CentralAchievements centralAchievements;

    public AchievementService() {

        try {
            FileInputStream fis = new FileInputStream(CHECKIN_ACHIEVEMENTS_LOCATION);
            ObjectInputStream ois = new ObjectInputStream(fis);
            checkins = (Map<String, CheckinAchievement>)ois.readObject();
            ois.close();
            fis.close();
        }
        catch (Exception e) {
            checkins = new HashMap<>();
        }
        try {
            FileInputStream fis = new FileInputStream(PAYMENT_ACHIEVEMENTS_LOCATION);
            ObjectInputStream ois = new ObjectInputStream(fis);
            payments = (Map<String, PaymentAchievement>)ois.readObject();
            ois.close();
            fis.close();
        }
        catch (Exception e) {
            payments = new HashMap<>();
        }
        try {
            FileInputStream fis = new FileInputStream(CENTRAL_ACHIEVEMENTS_LOCATION);
            ObjectInputStream ois = new ObjectInputStream(fis);
            centralAchievements = (CentralAchievements)ois.readObject();
            ois.close();
            fis.close();
        }
        catch (Exception e) {
            centralAchievements = new CentralAchievements();
        }
    }

    public AchievementAward getPossibleAchievement(String user) {
//        AchievementAward award = receivedAchievements.get(user);
//        if (award != null) {
//            receivedAchievements.remove(user);
//        }
//
//        if ("Erik Visser".equalsIgnoreCase(user) || "Peter van de Werken".equalsIgnoreCase(user)) {
//            return null;
//        }
//
//        return award;
        return null;
    }

    public void processCheckin(String user, int plin) {
//        if (!checkins.containsKey(user)) {
//            checkins.put(user, new CheckinAchievement());
//        }
//        CheckinAchievement achievementCounter = checkins.get(user);
//
//        AchievementAward award = achievementCounter.performCheckin(plin, centralAchievements);
//        if (award != null) {
//            receivedAchievements.put(user, award);
//        }
//        save();
    }

    public void processPayment(String user, double amount) {
        if (!payments.containsKey(user)) {
            payments.put(user, new PaymentAchievement());
        }
        PaymentAchievement achievementCounter = payments.get(user);

        AchievementAward award = achievementCounter.performPayment(amount, centralAchievements);
        if (award != null) {
            receivedAchievements.put(user, award);
        }
        save();
    }

    public void save() {
        saveObject(checkins, CHECKIN_ACHIEVEMENTS_LOCATION);
        saveObject(payments, PAYMENT_ACHIEVEMENTS_LOCATION);
        saveObject(centralAchievements, CENTRAL_ACHIEVEMENTS_LOCATION);
    }

    private void saveObject(Object toSave, String location) {
        try {
            FileOutputStream fout = new FileOutputStream(location);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(toSave);
            oos.close();
            fout.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
