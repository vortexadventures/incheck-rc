package org.n1.va.incheck.server.rest.extract;


import org.n1.va.incheck.server.rest.util.SpaceUtil;

/**
 * ExtractRow table rows
 */
public class ExtractRow {

    public String plin;
    public String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPlin() {
        return plin;
    }

    public void setPlin(String plin) {
        this.plin = SpaceUtil.fourLeadingSpaces(plin);
    }
}
