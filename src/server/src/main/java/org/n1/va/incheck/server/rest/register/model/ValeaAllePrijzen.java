package org.n1.va.incheck.server.rest.register.model;

/**
 * This class combines the event prijzen and the jaarprijzen.
 */
public class ValeaAllePrijzen {

    public final ValeaEventPrijzen eventPrijzen;
    public final ValeaJaarPrijzen jaarPrijzen;

    public ValeaAllePrijzen(ValeaEventPrijzen eventPrijzen, ValeaJaarPrijzen jaarprijzen) {
        this.eventPrijzen = eventPrijzen;
        this.jaarPrijzen = jaarprijzen;
    }
}
