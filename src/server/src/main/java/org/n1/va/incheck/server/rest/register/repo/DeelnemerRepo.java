package org.n1.va.incheck.server.rest.register.repo;

import org.n1.va.incheck.server.rest.register.jsonmodel.JsonNewPlayer;
import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DeelnemerRepo extends AbstractRepo {

    @Autowired
    public DeelnemerRepo(DataSource dataSource) {
        super(dataSource);
    }

    public ValeaDeelnemer getDeelnemer(int plin) {
        ValeaDeelnemer deelnemer = this.template.queryForObject(
                "SELECT" +
                        " id, plin, concat(trim(concat_ws(' ', voornaam, tussenvoegsels)), ' ', achternaam) name," +
                        " email_adres email, geboortedatum, opmerkingen, lid_start, lid_eind" +
                        " FROM deelnemers" +
                        " WHERE plin = ?",
                new ParameterizedRowMapper<ValeaDeelnemer>() {
                    @Override
                    public ValeaDeelnemer mapRow(ResultSet rs, int row) throws SQLException {
                        ValeaDeelnemer info = new ValeaDeelnemer();
                        info.id = rs.getLong("id");
                        info.plin = rs.getString("plin");
                        info.name = rs.getString("name");
                        info.dateOfBirth = rs.getDate("geboortedatum");
                        info.email = rs.getString("email");
                        info.opmerking = rs.getString("opmerkingen");
                        info.lid_start = rs.getDate("lid_start");
                        info.lid_eind = rs.getDate("lid_eind");

                        if (info.lid_start != null && info.lid_eind == null) {
                            info.member = true;
                        }

                        return info;
                    }
                },
                plin);

        return deelnemer;
    }

    public void updateLidStart(int plin, Date lidStart) {
        String sql = "UPDATE deelnemers SET lid_start=? WHERE plin=?";
        int updated = template.update(sql, lidStart, plin);
        assertUpdatedExactlyOneRow(updated, sql);
        updateLastUpdated(plin);
    }

    public void updateLidEind(int plin, Date lidEind) {
        String sql = "UPDATE deelnemers SET lid_eind=? WHERE plin=?";
        int updated = template.update(sql, lidEind, plin);
        assertUpdatedExactlyOneRow(updated, sql);
        updateLastUpdated(plin);
    }

    public void updateLidmaatschapsformulierOntvangen(int plin, boolean value) {
        String sql = "UPDATE deelnemers SET lidm_formulier_ontvangen=? WHERE plin=?";
        int updated = template.update(sql, value, plin);
        assertUpdatedExactlyOneRow(updated, sql);
        updateLastUpdated(plin);
    }

    public void updateDob(int plin, Date dob) {
        String sql = "UPDATE deelnemers SET geboortedatum=? WHERE plin=?";
        int updated = template.update(sql, dob, plin);
        assertUpdatedExactlyOneRow(updated, sql);
        updateLastUpdated(plin);
    }

    public void updateEmail(int plin, String email) {
        String sql = "UPDATE deelnemers SET email_adres=? WHERE plin=?";
        int updated = template.update(sql, email, plin);
        assertUpdatedExactlyOneRow(updated, sql);
        updateLastUpdated(plin);
    }

    public void updateLastUpdated(int plin) {
        String sql = "UPDATE deelnemers SET last_updated=? WHERE plin=?";
        int updated = template.update(sql, new Date(), plin);
        assertUpdatedExactlyOneRow(updated, sql);
    }

    public int getNextPlin() {
		int plin = -1;

		int[] holes = new int[]{ 4559, 4888, 5000, 5500, 5900 };
		for(int max : holes) {
			plin = this.template.queryForObject(
					"select max(plin) from deelnemers where plin < ?",
					Integer.class, max) + 1;
			if(plin < max)
				return plin;
		}
		return plin;
    }

    public void createNewPlayer(int plin, JsonNewPlayer newPlayer) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("plin", plin);
        params.put("voornaam", newPlayer.firstName);
        params.put("tussenvoegsels", newPlayer.tussenVoegsel);
        params.put("achternaam", newPlayer.lastName);
        params.put("mv", newPlayer.gender);
        params.put("last_updated", new Date());
        params.put("voorkeur_post", false);
        params.put("lidm_formulier_ontvangen", false);



        String sql = "INSERT INTO deelnemers " +
                "( plin, voornaam, tussenvoegsels, achternaam, mv, " +
                "last_updated, voorkeur_post, lidm_formulier_ontvangen )" +
                " VALUES" +
                "( :plin, :voornaam, :tussenvoegsels, :achternaam, :mv, " +
                ":last_updated, :voorkeur_post, :lidm_formulier_ontvangen)";

        int created = namedTemplate.update(sql, params);
        assertCreatedExactlyOneRow(created, sql);
    }
}
