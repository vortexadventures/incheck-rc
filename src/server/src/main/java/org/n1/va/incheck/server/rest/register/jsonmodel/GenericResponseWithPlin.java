package org.n1.va.incheck.server.rest.register.jsonmodel;

/**
 * generic response with plin, for new player.
 */
public class GenericResponseWithPlin extends GenericResponse {

    private int plin;

    public GenericResponseWithPlin(boolean success, String message, int plin) {
        super(success, message);
        this.plin = plin;
    }

    public int getPlin() {
        return plin;
    }

    public void setPlin(int plin) {
        this.plin = plin;
    }
}
