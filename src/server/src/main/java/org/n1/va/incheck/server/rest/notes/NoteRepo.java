package org.n1.va.incheck.server.rest.notes;

import org.n1.va.incheck.server.rest.cashregister.model.CashMutation;
import org.n1.va.incheck.server.rest.register.model.ValeaDeelnemer;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repo for notes
 */
@Repository
public class NoteRepo extends AbstractRepo{

    @Autowired
    public NoteRepo(DataSource dataSource) {
        super(dataSource);
    }

    public void insert(Note note) {
        Map<String, Object> params = new HashMap<>();
        params.put("deelnemer_id", note.deelnemer_id);
        params.put("gebruiker", note.gebruiker);
        params.put("tekst", note.tekst);
        params.put("tijdstip", note.tijdstip);

        String sql = "INSERT INTO opmerkingen (deelnemer_id, tekst, gebruiker, tijdstip) " +
                "VALUES (:deelnemer_id, :tekst, :gebruiker, :tijdstip)";
        int created = namedTemplate.update( sql, params);
        assertCreatedExactlyOneRow(created, sql);
    }

    public List<Note> getNotes(ValeaDeelnemer deelnemer) {
        Map<String, Object> params = new HashMap<>();
        params.put("deelnemerId", deelnemer.id);
        String sql = "SELECT * FROM opmerkingen WHERE deelnemer_id = :deelnemerId ORDER by tijdstip DESC";
        return namedTemplate.query(sql, params, ParameterizedBeanPropertyRowMapper.newInstance(Note.class));
    }
}
