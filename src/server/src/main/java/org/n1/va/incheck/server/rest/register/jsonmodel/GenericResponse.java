package org.n1.va.incheck.server.rest.register.jsonmodel;

/**
 * Response to a payment request
 */
public class GenericResponse {

    public boolean success;
    public String message;

    public GenericResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
