package org.n1.va.incheck.server.rest.cashregister.repo;

import org.n1.va.incheck.server.rest.cashregister.model.CashMutation;
import org.n1.va.incheck.server.rest.register.model.ValeaBankBoeking;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repo for kasmutatie table.
 */
@Repository
public class CashRegisterRepo extends AbstractRepo {

    @Autowired
    public CashRegisterRepo(DataSource dataSource) {
        super(dataSource);
    }


    public void insert(CashMutation mutation) {
        Map<String, Object> params = new HashMap<>();
        params.put("kassa", mutation.kassa);
        params.put("bedrag", mutation.bedrag);
        params.put("omschrijving", mutation.omschrijving);
        params.put("uitgevoerd_door", mutation.uitgevoerd_door);
        params.put("tijdstip", mutation.tijdstip);
        params.put("kasboeking_id", mutation.kasboeking_id);

        String sql = "INSERT INTO kasmutatie (kassa, bedrag, omschrijving, uitgevoerd_door, tijdstip, kasboeking_id) " +
                "VALUES (:kassa, :bedrag, :omschrijving, :uitgevoerd_door, :tijdstip, :kasboeking_id)";
        int created = namedTemplate.update( sql, params);
        assertCreatedExactlyOneRow(created, sql);
    }

    public List<CashMutation> getMutations(Date cutoff, String kassa) {
        Map<String, Object> params = new HashMap<>();
        params.put("cutoff", cutoff);
        params.put("kassa", kassa);
        String sql = "SELECT * FROM kasmutatie WHERE tijdstip >= :cutoff AND kassa = :kassa ORDER BY tijdstip DESC";
        return namedTemplate.query(sql, params,
                ParameterizedBeanPropertyRowMapper.newInstance(CashMutation.class));
    }

    public double getTotal(Date cutoff, String kassa) {
        Double total = this.template.queryForObject(
            "SELECT sum(bedrag) totaal FROM kasmutatie WHERE kassa = ? AND tijdstip >= ?",
            new ParameterizedRowMapper<Double>() {
                public Double mapRow(ResultSet rs, int row) throws SQLException {
                    return rs.getDouble("totaal");
                }
            },
            kassa, cutoff);
        return total;
    }
}