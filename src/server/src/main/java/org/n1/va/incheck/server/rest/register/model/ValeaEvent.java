package org.n1.va.incheck.server.rest.register.model;


import java.util.Date;

public class ValeaEvent {

    public long id;
    public long deelnemer_id;

    public boolean crew;
    public boolean monster;
    public boolean ehbo;
    public boolean barlid;
    public boolean bestuur;
    public boolean k12;
    public boolean k16;
    public boolean speciaal_bedrag;

    public boolean inschrijvingsformulier;
    public boolean toegang_betaald;
    public boolean evenementlidmaatschap_betaald;

    public Integer tafels;
    public Integer banken;

    public int jaar;
    public int naam;

    public Date incheck_date;
    public Date uitcheck_date;

    public String describeRole() {
        if (speciaal_bedrag) {
            return "Speciaal";
        }
        if (crew) {
            return "Crew";
        }
        if (this.monster) {
            return "Monster";
        }
        if (ehbo) {
            return "EHBO";
        }
        if (barlid) {
            return "Barlid";
        }
        if (bestuur) {
            return "Bestuur";
        }
        if (k12) {
            return "Jeugde < 12";
        }
        if (k16) {
            return "Jeugd 12-15";
        }

        return "Speler";
    }

    public String getRoleCode() {
        if (speciaal_bedrag) {
            return "speciaal";
        }
        if (crew) {
            return ValeaEventConst.ROLE_CREW;
        }
        if (this.monster) {
            return ValeaEventConst.ROLE_MONSTER;
        }
        if (ehbo) {
            return "ehbo";
        }
        if (barlid) {
            return "barlid";
        }
        if (bestuur) {
            return "bestuur";
        }
        if (k12) {
            return "k12";
        }
        if (k16) {
            return "k16";
        }

        return "speler";
    }

    public void updateForNewRole(String role) {
        boolean updated = updateForRoleLenient(role);
        if (!updated) {
            throw new IllegalArgumentException("Unknown roleCode: \"" + role + "\"");
        }
    }

    public boolean updateForRoleLenient(String role) {
        crew = false;
        monster = false;
        ehbo = false;
        barlid = false;
        bestuur = false;
        k12 = false;
        k16 = false;
        speciaal_bedrag = false;

        if ("speler".equals(role)) {
            return true;
        }
        if ("crew".equals(role)) {
            crew = true;
            return true;
        }
        if ("monster".equals(role)) {
            monster = true;
            return true;
        }
        if ("ehbo".equals(role)) {
            ehbo = true;
            return true;
        }
        if ("barlid".equals(role)) {
            barlid = true;
            return true;
        }
        if ("bestuur".equals(role)) {
            bestuur = true;
            return true;
        }
        if ("k12".equals(role)) {
            k12 = true;
            return true;
        }
        if ("k16".equals(role)) {
            k16 = true;
            return true;
        }
        if ("speciaal".equals(role)) {
            speciaal_bedrag = true;
            return true;
        }
        return false;
    }

    public boolean roleIsSpeler() {
        boolean sometingElse = crew |monster |ehbo |barlid |bestuur |k12 |k16 |speciaal_bedrag;
        return !sometingElse;
    }

    public boolean roleEntersForFree() {
        return (bestuur || ehbo || barlid );
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeelnemer_id() {
        return deelnemer_id;
    }

    public void setDeelnemer_id(long deelnemer_id) {
        this.deelnemer_id = deelnemer_id;
    }

    public boolean isCrew() {
        return crew;
    }

    public void setCrew(boolean crew) {
        this.crew = crew;
    }

    public boolean isMonster() {
        return monster;
    }

    public void setMonster(boolean monster) {
        this.monster = monster;
    }

    public boolean isEhbo() {
        return ehbo;
    }

    public void setEhbo(boolean ehbo) {
        this.ehbo = ehbo;
    }

    public boolean isBarlid() {
        return barlid;
    }

    public void setBarlid(boolean barlid) {
        this.barlid = barlid;
    }

    public boolean isBestuur() {
        return bestuur;
    }

    public void setBestuur(boolean bestuur) {
        this.bestuur = bestuur;
    }

    public boolean isInschrijvingsformulier() {
        return inschrijvingsformulier;
    }

    public void setInschrijvingsformulier(boolean inschrijvingsformulier) {
        this.inschrijvingsformulier = inschrijvingsformulier;
    }

    public boolean isToegang_betaald() {
        return toegang_betaald;
    }

    public void setToegang_betaald(boolean toegang_betaald) {
        this.toegang_betaald = toegang_betaald;
    }

    public boolean isEvenementlidmaatschap_betaald() {
        return evenementlidmaatschap_betaald;
    }

    public void setEvenementlidmaatschap_betaald(boolean evenementlidmaatschap_betaald) {
        this.evenementlidmaatschap_betaald = evenementlidmaatschap_betaald;
    }

    public boolean isK12() {
        return k12;
    }

    public void setK12(boolean k12) {
        this.k12 = k12;
    }

    public boolean isK16() {
        return k16;
    }

    public void setK16(boolean k16) {
        this.k16 = k16;
    }

    public boolean isSpeciaal_bedrag() {
        return speciaal_bedrag;
    }

    public void setSpeciaal_bedrag(boolean speciaal_bedrag) {
        this.speciaal_bedrag = speciaal_bedrag;
    }

    public Integer getTafels() {
        return tafels;
    }

    public void setTafels(Integer tafels) {
        this.tafels = tafels;
    }

    public Integer getBanken() {
        return banken;
    }

    public void setBanken(Integer banken) {
        this.banken = banken;
    }

    public int getJaar() {
        return jaar;
    }

    public void setJaar(int jaar) {
        this.jaar = jaar;
    }

    public int getNaam() {
        return naam;
    }

    public void setNaam(int naam) {
        this.naam = naam;
    }

    public Date getIncheck_date() {
        return incheck_date;
    }

    public void setIncheck_date(Date incheck_date) {
        this.incheck_date = incheck_date;
    }

    public Date getUitcheck_date() {
        return uitcheck_date;
    }

    public void setUitcheck_date(Date uitcheck_date) {
        this.uitcheck_date = uitcheck_date;
    }


}
