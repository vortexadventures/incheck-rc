package org.n1.va.incheck.server.rest.register.repo;

import org.n1.va.incheck.server.rest.register.model.ValeaEvent;
import org.n1.va.incheck.server.rest.register.model.ValeaEventIdentifier;
import org.n1.va.incheck.server.rest.register.model.ValeaEventPrijzen;
import org.n1.va.incheck.server.rest.register.model.ValeaJaarPrijzen;
import org.n1.va.incheck.server.rest.util.AbstractRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Repository
public class PrijzenRepo extends AbstractRepo {


    private Map<String, ValeaEventPrijzen> prijzenCache = new HashMap<String, ValeaEventPrijzen>();

    private Map<Integer, ValeaJaarPrijzen> yearPriceCache = new HashMap<Integer, ValeaJaarPrijzen>();

    @Autowired
    public PrijzenRepo(DataSource dataSource) {
        super(dataSource);
    }


    public ValeaEventPrijzen getPrijzen(ValeaEventIdentifier event) {
        return getPrijzen(event.jaar, event.eventName);
    }

    public ValeaEventPrijzen getPrijzen(ValeaEvent event) {
        return getPrijzen(event.jaar, event.naam);
    }

    private ValeaEventPrijzen getPrijzen(int jaar, int eventName) {
        String prijzenKey = jaar + ":" + eventName;

        if (prijzenCache.containsKey(prijzenKey)) {
            return prijzenCache.get(prijzenKey);
        }
        else {
            ValeaEventPrijzen prijzen = fetchEventPrijzen(jaar, eventName);
            prijzenCache.put(prijzenKey, prijzen);
            return prijzen;
        }
    }


    private ValeaEventPrijzen fetchEventPrijzen(int jaar, int eventName) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jaar", jaar);
        params.put("eventName", eventName);
        ValeaEventPrijzen prijzen = namedTemplate.queryForObject(
                "select " +
                        "jaar, event, prijs_poort, prijs_voor_uiterlijke_datum, prijs_voor, prijs_crew, " +
                        "prijs_k12_15_voor, prijs_k12_15_poort, prijs_k11_jonger_voor, prijs_k11_jonger_poort, " +
                        "prijs_jaarpakket, prijs_jaarpakket_crew " +
                        "from event_toegangsprijzen " +
                        "where event = :eventName " +
                        "and jaar= :jaar"
                , params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaEventPrijzen.class)
        );
        return prijzen;
    }

    public ValeaJaarPrijzen getJaarPrijzen(int year) {
        if (yearPriceCache.containsKey(year)) {
            return yearPriceCache.get(year);
        }
        else {
            ValeaJaarPrijzen prices = fetchJaarPrijzen(year);
            yearPriceCache.put(year, prices);
            return prices;
        }
    }

    private ValeaJaarPrijzen fetchJaarPrijzen(int year) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("jaar", year);
        ValeaJaarPrijzen prijzen = namedTemplate.queryForObject(
                "select " +
                        "jaar, evenementlidmaatschap, jaarlidmaatschap, jaarpakket, crew_jaarpakket, " +
                        "jaarpakket_uiterlijke_datum " +
                        "from jaar_toegangsprijzen " +
                        "where jaar= :jaar"
                , params,
                ParameterizedBeanPropertyRowMapper.newInstance(ValeaJaarPrijzen.class)
        );
        return prijzen;
    }

}
