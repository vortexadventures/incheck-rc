package org.n1.va.incheck.server.rest.util;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Redo Logging variant of NamedParameterJdbcTemplate.
 */
public class RedoLogNamedTemplate {

    private NamedParameterJdbcTemplate namedTemplate;
    private RedoLogger redoLogger;

    public RedoLogNamedTemplate(NamedParameterJdbcTemplate namedTemplate, RedoLogger redoLogger) {
        super();
        this.namedTemplate = namedTemplate;
        this.redoLogger =redoLogger;
    }

    public int update(java.lang.String sql, java.util.Map<java.lang.String,?> paramMap) {
        redoLogger.redoLog(sql, paramMap);
        return namedTemplate.update(sql, paramMap);
    }

    public <T> T queryForObject(String sql, Map<String, Object> params, ParameterizedBeanPropertyRowMapper<T> mapper) {
        return namedTemplate.queryForObject(sql, params, mapper);
    }


    public <T> List<T> query(String sql, Map<String, Object> params, ParameterizedBeanPropertyRowMapper<T> mapper) {
        return namedTemplate.query(sql, params, mapper);
    }

}
