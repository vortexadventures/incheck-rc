<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<body>
	<h2>Vortex Adventures - deelnemers op ${parsedDate}</h2>
    <c:if test="${errorMessage != null}">
        Error: ${errorMessage}
    </c:if>
    <c:if test="${errorMessage == null}">
        <pre>Totaal aantal deelnemers op deze dag: ${contestants.size()}.</pre>
        <pre><c:forEach items="${contestants}" var="row">${row.number},${row.plin},${row.name}<br></c:forEach></pre>
    </c:if>
</body>
</html>