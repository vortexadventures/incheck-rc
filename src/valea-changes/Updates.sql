alter table events add column incheck_date datetime;
alter table events add column uitcheck_date datetime;

alter table boeking_event add column product text;
alter table boeking_event add column omschrijving text;
ALTER TABLE boeking_event CHANGE betaald_voor betaald_voor enum('toegang', 'catering', 'meubilair', 'boekjes', 'overigen');


create table kasmutatie (id int primary key auto_increment, kassa varchar(100) not null, bedrag float not null, omschrijving text not null, uitgevoerd_door varchar(100) not null, tijdstip datetime not null, kasboeking_id int null);

create table opmerkingen (id int primary key auto_increment, deelnemer_id int not null, tekst text not null, gebruiker varchar(100) not null, tijdstip datetime not null);




ALTER TABLE deelnemers MODIFY voornaam VARCHAR(100);
ALTER TABLE deelnemers MODIFY achternaam VARCHAR(100);
DROP INDEX voornaam on deelnemers;
DROP INDEX achternaam on deelnemers;
ALTER TABLE deelnemers ENGINE=InnoDB;
ALTER TABLE deelnemers ADD INDEX (voornaam);
ALTER TABLE deelnemers ADD INDEX (achternaam);

ALTER TABLE boeking_event ENGINE=InnoDB;
ALTER TABLE crediteuren ENGINE=InnoDB;
ALTER TABLE events ENGINE=InnoDB;
ALTER TABLE event_names ENGINE=InnoDB;
ALTER TABLE event_toegangsprijzen  ENGINE=InnoDB;
ALTER TABLE jaargegevens ENGINE=InnoDB;
ALTER TABLE jaar_toegangsprijzen ENGINE=InnoDB;
ALTER TABLE kasboeking ENGINE=InnoDB;
ALTER TABLE kassalijst_import ENGINE=InnoDB;
ALTER TABLE rabo_import ENGINE=InnoDB;
ALTER TABLE schulden ENGINE=InnoDB;
ALTER TABLE sessiegegevens ENGINE=InnoDB;
ALTER TABLE teksten ENGINE=InnoDB;
ALTER TABLE kasmutatie ENGINE=InnoDB;
ALTER TABLE opmerkingen ENGINE=InnoDB;


