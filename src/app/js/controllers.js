'use strict';

/* Controllers */


angular.module('myApp.controllers', []).
    controller('NavBarController', [
    'loginService', 'notyService', 'eventService', '$scope', '$rootScope', '$location', '$cookies',
    function (loginService, notyService, eventService, $scope, $rootscope, $location, $cookies) {

        // ----------------------------------------------------------//
        // --------------------- NavBarController -------------------//
        // ----------------------------------------------------------//

        eventService.getEventNameCallback(function(eventName) {
             $scope.name = eventName;
        });
        $scope.page = "search";

        $rootscope.$on('changePage', function (event, pageTo) {
            $scope.page = pageTo;
            $scope.user = loginService.getUser();
            $scope.loggedIn = loginService.isLoggedIn();
            $scope.cashRegiser = $cookies.registerName;
        });
        $rootscope.$on("$routeChangeStart", function (event, next, current) {
            if (!loginService.isLoggedIn()) {
                $location.path('/login');
            }
        });
        $scope.user = loginService.getUser();
        $scope.logout = function() {
            loginService.logout();
            $scope.user = "";
            $scope.loggedIn = false;
            $location.path('/logout');
        };
        $scope.loggedIn = false;
        $scope.cashRegiser = $cookies.registerName;
        $scope.openPrijzen = function() {
            window.open("/Prijzen.html", "_blank");
        }

        // This method cannot use angular scoped objects the regular way because the method
        // is called from outside the context of this controller. Hence the clunky use of $.ajax and $scope.
        function checkSessionTimeOut() {
            if (loginService.isLoggedIn()) {
                $.ajax({
                    url: 'server/session/isTimeout',
                    success: function (data) {
                        if (data) {
                            notyService.info("Gebruiker " + loginService.getUser() + " is automatisch uitgelogd." );
                            var appElement = document.querySelector('[ng-controller=NavBarController]');
                            var $scope = angular.element(appElement).scope();
                            $scope.$apply(function() {
                                $scope.logout();
                            });
                        }
                    }
                });
            }
        }
        setInterval(checkSessionTimeOut, 5000);
    }])
    .controller('AchievementController', ['achievementService', 'notyService', '$scope', '$location', '$cookies', '$http', '$routeParams',
        function (achievementService, notyService, $scope, $location, $cookies, $http, $routeParams) {

            // ----------------------------------------------------------//
            // -------------- AchievementController --------------------//
            // ----------------------------------------------------------//

            $scope.$emit('changePage', "achievement");
            $scope.image = achievementService.image;
            // nothing to see here...
    }])
    .controller('LoginController', ['loginService', 'notyService', '$scope', '$location', '$cookies', '$http',
        function (loginService, notyService, $scope, $location, $cookies, $http) {

        // ----------------------------------------------------------//
        // --------------------- LoginController --------------------//
        // ----------------------------------------------------------//

        $scope.$emit('changePage', "login");

        $scope.keyPress = function(event) {
            if (event.keyCode != 13) {
                return;
            }
            $scope.login();
        };

        $scope.login = function() {
            var input = $("#query").val();
            if (input && input.length <= 1) {
                return;
            }

            if ($scope.registerName === 'choose' ) {
                notyService.error("Kies de kassalade die bij dit systeem hoort.");
                return;
            }
            else {
                $http.get('server/register/get/' + input).success(function (data) {
                    if (data && data.plin) {
                        loginService.login(data.name);
                        $cookies.user = data.name;
                        $http.post("server/cashregister/check", $scope.registerName).
                            success(function(data) {
                                $location.path("/search");
                            }).
                            error(function(){
                                notyService.techError("Configureren van de kassalade is mislukt. PANIEK! Vooral niet peeuwen!");
                            });
                    }
                    else {
                        notyService.error("Voer je PLIN in a.u.b.");
                    }
                    }).
                    error(function() {
                        notyService.error("Voer je PLIN in a.u.b.");
                    });
            }
        }

        // specific test for truthy
        if ($cookies.registerName) {
            $scope.registerName = $cookies.registerName;
        }
        else {
            $scope.registerName = 'choose';
        }


        $scope.chooseRegister = function (name) {
            if ( name !== 'choose') {
                $cookies.registerName = name;
            }
        }
    }]).
    controller('NewPlayerController', ['notyService', '$scope', '$http', '$location',
        function (notyService, $scope, $http, $location) {
        $scope.$emit('changePage', "newPlayer");

        $scope.submit = function() {
            var message = {
                firstName: $scope.firstName,
                tussenVoegsel: $scope.tussenVoegsel,
                lastName: $scope.lastName,
                dob: $scope.dob,
                email: $scope.email,
                gender: $scope.gender
            }
            if (!message.firstName || !message.lastName || !message.dob || !message.gender) {
                notyService.error("Invoer incompleet");
                return;
            }

            $http.post('server/register/new', message).
                success(function(data) {
                    var success = data.success;
                    if (success) {
                        notyService.info(data.message);
                        $location.path("/incheck/" + data.plin);
                    }
                    else {
                        notyService.error(data.message);
                    }
                }).
                error(function(data) {
                    notyService.techError("Aanmaken nieuwe deelnemer mislukt.");
                });

            $scope.gender = "V";
        }

    }]).
    controller('SearchController', ['notyService', 'achievementService', '$scope', '$http', '$location',
        function (notyService, achievementService, $scope, $http, $location) {

        // ----------------------------------------------------------//
        // --------------------- SearchController -------------------//
        // ----------------------------------------------------------//

        $scope.$emit('changePage', "search");

        $http.get('server/extract/all').success(function (data) {
            $scope.contestants = data;
        });

        $scope.keyPress = function(event) {
            if (event.keyCode != 13) {
                return;
            }
            $scope.go();
        };

        $scope.register = function (plin) {
            if (plin) {
                $location.path("/incheck/" + plin);
            }
        }

        $scope.go = function() {
            var plin = $("#result-0").text().trim();
            $scope.register(plin);
        }

        $scope.new = function() {
            $location.path("/new");
        }

        // Check for achievements
        $http.get('server/achievements/get').success(function (data) {
            if (data) {
                notyService.success(data.message);
                achievementService.image = data.image;
                $location.path("/achievement");
            }
        });

    }])
    .controller('ReportController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

        // ----------------------------------------------------------//
        // --------------------- ReportController -------------------//
        // ----------------------------------------------------------//

        $scope.$emit('changePage', "report");

        $scope.keyPress = function(event) {
            if (event.keyCode != 13) {
                return;
            }
            $scope.openReport();
        };

        $scope.openReport = function() {
            var input = $("#query").val();
            if (input) {
                window.open("server/report/occupants/" + input, "_blank");
            }
        }
    }])
    .controller('IncheckController', ['loginService', 'notyService', 'eventService', '$scope', '$http', '$routeParams', '$route', '$location',
        function (loginService, notyService, eventService, $scope, $http, $routeParams, $route, $location) {

            // ----------------------------------------------------------------------------------------------------------------//
            // --------------------- IncheckController ------------------------------------------------------------------------//
            // ----------------------------------------------------------------------------------------------------------------//

        $scope.$emit('changePage', "incheck");
        $scope.overRule = false;
        $scope.plin = $routeParams.plin;
        $scope.contestant = undefined;

        $scope.customItemPurpose = 'choose';
        $scope.customItemButtonDisabled = true;


            // ------------------ Fetch data ----------------------- //

        $http.get('server/register/get/' + $scope.plin).success(function (data) {
            $scope.contestant = data;
            var nowMillis = new Date().getTime();
            var dob = data.dateOfBirthMillis;
            if (dob) {
                $scope.contestant.dateOfBirth = new Date(dob);
                var ageMillis = nowMillis - dob;
                var age = Math.floor(ageMillis / (1000 * 60 * 60 * 24 * 365));
                $scope.contestant.age = age;
                $scope.contestant.dateOfBirthDefined = true;
            }
            else {
                $scope.contestant.dateOfBirthDefined = false;
            }
        });

       $scope.nextPlin = function() {
           $http.get('server/register/nextPlin/' + $scope.plin).success(function(data) {
               $location.path("/incheck/" + data);
           });
       }


        function changeMembershipConfirmed() {
            $http.get('server/register/changeMembership/' + $scope.plin).
                success(function (data) {
                    notyService.info(data);
                    $route.reload();
                }).
                error(function() {
                    notyService.techError("Wijzigen lidmaatschapstatus mislukt.");
                });
            $scope.$apply();
        };

        $scope.changeMembership = function() {
            var text = "Bevestig het ";
            text += ($scope.contestant.member)? "stoppen" : "starten";
            text += " van het lidmaatschap van " + $scope.contestant.name + ".";
            if (!$scope.contestant.member) {
                text += " Bevestig tevens de ontvangst van het getekende lidmaatschapsformulier.";
            }
            notyService.confirm(text, changeMembershipConfirmed );
        }

        $scope.updateDob = function() {
            var dobv = $('#dobValue').val();
            var message = {
                plin: $scope.plin,
                input: dobv
            }
            $http.post('server/register/updateDob', message).
                success(function(data) {
                    if (data.success) {
                        notyService.infoFast("Geboortedatum bijgewerkt");
                        $route.reload();
                    }
                    else {
                        notyService.error(data.message);
                    }
                }).
                error(function() {
                    notyService.techError("Geboortedatum bijwerken mislukt.");
                });
        }

        $scope.updateEmail = function() {
            var value = $('#emailValue').val();
            var message = {
                plin: $scope.plin,
                input: value
            }
            $http.post('server/register/updateEmail', message).
                success(function(data) {
                    if (data.success) {
                        notyService.infoFast("Email bijgewerkt");
                        $route.reload();
                    }
                    else {
                        notyService.error(data.message);
                    }
                }).
                error(function() {
                    notyService.techError("Email bijwerken mislukt.");
                });
        }

        // ------------------ CART -----------------------//
        $scope.cart = { total: 0, items: [
        ]};
        $scope.pay = {cash: false, pin: false};

        $scope.displayItem = function (item) {
            return item.name;
        }

        $scope.removeItem = function (item) {
            var index = $.inArray(item, $scope.cart.items);
            $scope.cart.items.splice(index, 1);
            updateCart();
        }

        function updateCart() {
            $scope.cart.total = 0;
            var pinRequired = false;
            for (var i = 0; i < $scope.cart.items.length; i += 1) {
                var item = $scope.cart.items[i]
                $scope.cart.total += item.total;
                if (item.type === 'pin') {
                    pinRequired = true;
                }
            }
            if ($scope.cart.total === 0) {
                $scope.pay.cash = false;
                $scope.pay.pin = false;
            }
            else {
                $scope.pay.pin = true;
                $scope.pay.cash = !pinRequired;
            }
        }

        // --------------------- Purchase options -------------------//

            eventService.getShopLinesCallback(function(shopLines){
                $scope.shop = shopLines;
            })
//            $scope.shop = { lines1: [
//                { line: 'item', name: "Evenementslidmaatschap", role: "event", amount: 500, purpose: "toegang", icon: "icon-leaf", style: {}},
//                { line: 'separator'},
//                { line: 'item', name: "Speler", amount: 6500, role: "speler", purpose: "toegang", icon: "icon-user", style: {}},
//                { line: 'item', name: "Speler (12-15)", role: "k16", amount: 3250, purpose: "toegang", icon: "icon-user", style: {}},
//                { line: 'item', name: "Crew", amount: 4500, role: "crew", purpose: "toegang", icon: "icon-bookmark", style: {}},
//                { line: 'item', name: "Overnachting donderdag", amount: 750, purpose: "overigen", icon: "icon-star", style: {visibility: 'hidden'}},
//                { line: 'separator'},
//                { line: 'item', name: "Tafel", amount: 500, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
//                { line: 'item', name: "Bank", amount: 250, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden' }}
//            ], lines2: [
//                { line: 'item', name: "Verenigingslidmaatschap", role: "member", purpose: "toegang", amount: 1500, icon: "icon-leaf", style: {}},
//                { line: 'separator'},
//                { line: 'item', name: "Monster", amount: 4500, role: "monster", purpose: "toegang", icon: "icon-bookmark", style: {}},
//                { line: 'item', name: "Speler (onder de 12)", role: "k12", amount: 0, purpose: "toegang", icon: "icon-user", style: {}},
//                { line: 'item', name: "Dagpas", amount: 2000, role: "speciaal", purpose: "toegang", icon: "icon-user", style: {}},
//                { line: 'filler'},
//                { line: 'separator'},
//                { line: 'item', name: "T-shirt", amount: 1000, purpose: "overigen", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
//                { line: 'filler'}
//            ]};


        $scope.addItem = function (item) {
            var cartItem = { name: item.name, total: item.amount, role: item.role, purpose: item.purpose};
            $scope.cart.items.push(cartItem);
            updateCart();
        }

        $scope.addCustomItem = function() {
            if ($scope.customItemButtonDisabled) {
                return;
            }

            var name = $('#customItemName').val();
            var amountInput = $('#customItemAmount').val();
            var amount = parseAmount(amountInput);
            var customItemPurpose = $('#customItemPurpose').val();

            if (name && amount) {
                var cartItem = { name: name, total: amount, role: "custom", purpose: customItemPurpose};
                $scope.cart.items.push(cartItem);
                updateCart();
                $('#customItemName').val('');
                $('#customItemAmount').val('');
            }
        }

        $scope.evaluateCustomItemPurpose = function() {
            $scope.customItemButtonDisabled = ( $('#customItemPurpose').val() == 'choose');
        }

        $scope.addPinAmount = function() {
            var amountInput = $('#pinAmount').val();
            var amount = parseAmount(amountInput);

            if (amount) {
                var cartItem = { name: 'Geld opnemen met PIN', total: amount, type:'pin', role: "pin", purpose: "overigen"};
                $scope.cart.items.push(cartItem);
                updateCart();
                $('#pinAmount').val('');
            }
        }

        function parseAmount(value) {
            var val = value.replace(',', '.');

            var decimalIndex = val.indexOf('.');
            if ( decimalIndex === -1) {
                return 100 * parseInt(val);
            }
            else {
                var decimals = val.length - decimalIndex - 1;
                if (decimals === 1) {
                    val += '0';
                }
                return parseInt(val.replace('.', ''));
            }
        }

        $scope.payWithPin = function() {
            if ($scope.pay.pin) {
                pay(false);
            }
        }

        $scope.payWithCash = function() {
            if ($scope.pay.cash) {
                pay(true);
            }
        }

        function pay(cash) {
            var paymentMessage = {
                totalAmount: $scope.cart.total,
                cash: cash,
                items: $scope.cart.items,
                plin: $scope.contestant.plin,
                cashier: loginService.getUser()
            };
//            var paymentMessage = { aap: cash};
//            $http.post('server/register/pay' + $scope.plin).success(function (data) {
            $http.post('server/register/pay', paymentMessage).
                success(function (data) {
                    if (data.success) {
                        $route.reload();
                    }
                    noty({
                        text: data.message,
                        layout: 'topCenter',
                        type: (data.success) ? 'information' : 'error',
                        timeout: (data.success) ? 5000 : null
                        });
                    }).
                error(function() {
                    notyService.techError("Verwerken van de betaling mislukt.");
                });
        }

        // ---------------------- HISTORY ------------------------------------- //

        $scope.paymentHistory = [];



        $http.get('server/register/paymentHistory/' + $scope.plin).
            success(function (data) {
                $scope.paymentHistory = data;
            }).
            error(function (data) {
                notyService.techErrorFast("Ophalen van de betaalgeschiedenis mislukt.");
            });

        // -------------------------------------------------------------------- //

        var formatAmount = function (amount) {

            var fraction = Math.abs(Math.floor(amount % 100));
            var whole = Math.floor(amount / 100);
            if (amount < 0 && fraction > 0) {
                whole += 1;
            }

            var formatted = "" + whole + ",";
            if (fraction === 0 ) {
                formatted += "-";
            }
            else {
				if (fraction < 10) {
					formatted += "0";
				}
                formatted += fraction;
            }
            return formatted;
        }

        $scope.formatAmount = formatAmount;

        $scope.formatDate = function(millis) {
            var date = new Date(millis);
            return date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
        }

        // --------------------- Check in and out -------------------//

        $scope.checkin = function() {
            var url = 'server/register/checkin/' + $scope.contestant.plin + "/" + $scope.contestant.roleCode;
            $http.get(url).
                success(function (data) {
                    notyService.infoFast( $scope.contestant.name + " is ingechecked.");
                    $route.reload();
                }).
                error(function() {
                    notyService.techError("Inchecken mislukt.");
                });
        }


        $scope.checkinOverride = function() {
            var roleCode = $("#roleSelector option:selected").val();
            var roleDescription = $("#roleSelector option:selected").text();
            var text = "Bevestig het inchecken van " + $scope.contestant.name + " als " + roleDescription;

            function checkinOverrideConfirm() {
                $scope.contestant.roleCode = roleCode;
                $scope.checkin();
                $scope.$apply();
            }

            notyService.confirm(text, checkinOverrideConfirm );
         }

        $scope.checkout = function() {
            var url ='server/register/checkout/' + $scope.contestant.plin;
            $http.get(url).
                success(function (data) {
                    notyService.info($scope.contestant.name + " is uitgechecked.");
                    $route.reload();
                }).
                error(function() {
                    notyService.techError("Uitchecken mislukt");
                });
        }




        $scope.done = function () {
            $location.path("/search");
        }

        //--------------------------------------------------------------------------------------------

        $scope.removeRole = function() {
            $scope.contestant.roleCode = null;
            $scope.contestant.roleDescription = null;
            $scope.contestant.paidInFull.event = false;
        }

        //------------------------------- Notes -------------------------------------------------

        $scope.notes = [];

        $http.get('server/notes/get/' + $scope.plin).
            success(function (data) {
                $scope.notes = data;
            }).
            error(function() {
                notyService.techErrorFast("Notities konden niet worden opgehaald.");
            });

        $scope.addNote = function() {
            if (!$scope.noteText) {
                return;
            }
            var url ='server/notes/add';
            var note = {
                user: loginService.getUser(),
                text: $scope.noteText,
                plin: $scope.contestant.plin
            };

            $http.post(url, note).
                success(function (data) {
                    notyService.infoFast("Notitie opgeslagen voor het nageslacht.");
                    $scope.notes = data;
                    $scope.noteText = "";
                }).
                error(function() {
                    notyService.techError("Notitie opgeslaan mislukt.");
                });
        }

    }]).
    controller('CashRegisterController', ['loginService', 'notyService', 'eventService', '$scope', '$http', '$route',
        function (loginService, notyService, eventService, $scope, $http, $route) {

        // ----------------------------------------------------------//
        // ---------------- CashRegisterController ------------------//
        // ----------------------------------------------------------//

//        $scope.name = eventService.getEventName();
        $scope.$emit('changePage', "register");


        var url ='server/cashregister/report';
        $http.get(url).
            success(function (data) {
                $scope.mutaties = data.mutations;
                $scope.totalAmount = data.total;
            }).
            error(function() {
                notyService.techError("Ophalen van de kassamutaties mislukt.")
            });


        $scope.mutaties = [];

        $scope.totalAmount = 0;

        $scope.abs = Math.abs;

        $scope.mutateRegister = function () {
            var mutatie = {
                user: loginService.getUser(),
                reason: $scope.reason,
                amount: $scope.amount.replace(',', '.')
            }
            var url ='server/cashregister/mutate/';
            $http.post(url, mutatie).
                success(function (data) {
                    notyService.infoFast("Aangepast.");
                    $route.reload();
                }).
                error(function() {
                    notyService.techError("Aanpassing mislukt.")
                });

            $scope.reason = "";
            amount: $scope.amount = "";
        }

        $scope.positive = function (number) {
            return number >= 0;
        }


        $scope.printAmount = function (amount) {
            if (amount === 0) {
                return "&nbsp;(leeg)&nbsp;"
            }

            var text = (amount >= 0) ? '+ ' : '- ';
            var unsigned = Math.abs(amount);

            var whole = Math.floor(unsigned);
            var decimals = unsigned - whole;
            var decimalsTruncated = Math.floor(decimals * 100);

            text += (whole < 10) ? "&nbsp;" : "";
            text += (whole < 100) ? "&nbsp;" : "";
            text += (whole < 1000) ? "&nbsp;" : "";
            text += whole;
            if (whole === unsigned) {
                return text + ",-&nbsp;";
            }
            return text + "," + decimalsTruncated;
        }
    }])


;
