
// Prevent ENTER to submit forms unintentionally.
document.onkeypress = function(evt) {
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}
