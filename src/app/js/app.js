'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers', 'ngSanitize', 'ngCookies']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/search', {templateUrl: 'partials/search.html', controller: 'SearchController'});
    $routeProvider.when('/incheck/:plin', {templateUrl: 'partials/incheck.html', controller: 'IncheckController'});
    $routeProvider.when('/register', {templateUrl: 'partials/cashregister.html', controller: 'CashRegisterController'});
    $routeProvider.when('/report', {templateUrl: 'partials/report.html', controller: 'ReportController'});
    $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginController'});
    $routeProvider.when('/new', {templateUrl: 'partials/newPlayer.html', controller: 'NewPlayerController'});
    $routeProvider.when('/achievement', {templateUrl: 'partials/achievement.html', controller: 'AchievementController'});


    $routeProvider.otherwise({redirectTo: '/search'});
  }]);
