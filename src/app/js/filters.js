'use strict';

/* Filters */

angular.module('myApp.filters', []).
    filter('blockNoQuery', [function () {
        return function (array, input) {
            if (!input) {
                return [];
            }
            else {
                return array;
            }
        }
    }]).
    filter('iif', [function () {
        return function (input, trueValue, falseValue) {
            return input ? trueValue : falseValue;
        }
    }]).
    filter('stripLeadingZeroes', [function () {
        function stripZeros(input) {
            if (input.indexOf('0') != 0) {
                return input;
            }
            else {
                return stripZeros(input.slice(1));
            }
        }
        return function (input) {
            return stripZeros(input);
        }
    }]);