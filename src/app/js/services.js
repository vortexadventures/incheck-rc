'use strict';

angular.module('myApp.services', []).
    factory('loginService', function(){
        var user = "";

        return {
            login : function(name) {
                user = name;
            },
            logout : function() {
                user = "";
            },
            getUser : function() {
                return user;
            },
            isLoggedIn : function() {
                return user != "";
            }
        };
    }).

    factory('eventService', ['$http',
        function($http){

            var shopLines = null;

            return {
                getEventNameCallback: function(callbackFunction){
                    $http.get('/server/event/name').
                        success( callbackFunction );
                },

                getShopLinesCallback: function(callbackFunction) {
                    if (shopLines != null) {
                        callbackFunction(shopLines);
                        return;
                    }

                    $http.get('/server/event/prices').
                        success( function(prices){

                            var event = prices.eventPrijzen;
                            var jaar = prices.jaarPrijzen;

                            // shop layout regular: without year package
                            // var shop = { lines1: [
                            //     { line: 'item', name: "Evenementslidmaatschap", role: "event", amount: 100 * jaar.evenementlidmaatschap, purpose: "toegang", icon: "icon-leaf", style: {}},
                            //     { line: 'separator'},
                            //     { line: 'item', name: "Speler", amount: 100 * event.prijs_poort, role: "speler", purpose: "toegang", icon: "icon-user", style: {}},
                            //     { line: 'item', name: "Speler (12-15)", role: "k16", amount: 100 * event.prijs_k12_15_poort, purpose: "toegang", icon: "icon-user", style: {}},
                            //     { line: 'item', name: "Crew", amount: 100 * event.prijs_crew_poort, role: "crew", purpose: "toegang", icon: "icon-bookmark", style: {}},
                            //     { line: 'item', name: "Overnachting (donderdag)", amount: 750, purpose: "overigen", icon: "icon-star", style: {visibility: 'hidden'}},
                            //     { line: 'separator'},
                            //     { line: 'item', name: "Tafel", amount: 500, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
                            //     { line: 'item', name: "Bank", amount: 500, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden' }}
                            // ], lines2: [
                            //     { line: 'item', name: "Verenigingslidmaatschap", role: "member", purpose: "toegang", amount: 100 * jaar.jaarlidmaatschap, icon: "icon-leaf", style: {}},
                            //     { line: 'separator'},
                            //     { line: 'item', name: "Monster", amount: 100 * event.prijs_monster_poort, role: "monster", purpose: "toegang", icon: "icon-bookmark", style: {}},
                            //     { line: 'item', name: "Speler (onder de 12)", role: "k12", amount: 100 * event.prijs_k11_jonger_poort, purpose: "toegang", icon: "icon-user", style: {}},
                            //     { line: 'item', name: "Dagpas", amount: 2000, role: "speciaal", purpose: "toegang", icon: "icon-user", style: {}},
                            //     { line: 'filler'},
                            //     { line: 'separator'},
                            //     { line: 'item', name: "T-shirt", amount: 1000, purpose: "overigen", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
                            //     { line: 'filler'}
                            // ]};

                            // Shop layout with year package

                            var shop = { lines1: [
                               { line: 'item', name: "Evenementslidmaatschap", role: "event", amount: 100 * jaar.evenementlidmaatschap, purpose: "toegang", icon: "icon-leaf", style: {}},
                               { line: 'separator'},
                               { line: 'item', name: "Speler", amount: 100 * event.prijs_poort, role: "speler", purpose: "toegang", icon: "icon-user", style: {}},
                               { line: 'item', name: "Speler (12-15)", role: "k16", amount: 100 * event.prijs_k12_15_poort, purpose: "toegang", icon: "icon-user", style: {}},
                               { line: 'item', name: "Speler (onder de 12)", role: "k12", amount: 100 * event.prijs_k11_jonger_poort, purpose: "toegang", icon: "icon-user", style: {}},
                               { line: 'item', name: "Dagpas", amount: 2000, role: "speciaal", purpose: "toegang", icon: "icon-user", style: {}},
                               { line: 'separator'},
                               { line: 'item', name: "Tafel", amount: 500, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
                               { line: 'item', name: "Bank", amount: 500, purpose: "meubilair", icon: "icon-shopping-cart", style: {visibility: 'hidden' }}
                            ], lines2: [
                               { line: 'item', name: "Verenigingslidmaatschap", role: "member", purpose: "toegang", amount: 100 * jaar.jaarlidmaatschap, icon: "icon-leaf", style: {}},
                               { line: 'separator'},
                               { line: 'item', name: "Crew", amount: 100 * event.prijs_crew_poort, role: "crew", purpose: "toegang", icon: "icon-bookmark", style: {}},
                               { line: 'item', name: "Monster", amount: 100 * event.prijs_monster_poort, role: "monster", purpose: "toegang", icon: "icon-bookmark", style: {}},
                               { line: 'item', name: "Overnachting (donderdag)", amount: 750, purpose: "overigen", icon: "icon-star", style: {visibility: 'hidden'}},
                               { line: 'filler'},
//                               { line: 'item', name: "Jaarpakket", amount: 16500, role: "speler", purpose: "toegang", icon: "icon-user", style: {}},
                               { line: 'separator'},
                               { line: 'item', name: "T-shirt", amount: 500, purpose: "overigen", icon: "icon-shopping-cart", style: {visibility: 'hidden'}},
                               { line: 'filler'}
                            ]};

                            shopLines = shop;
                            callbackFunction(shop);
                        });
                }
            }
        }]).

    factory('achievementService', function() {
        return {
            image: null
        };
    }).

    factory('notyService', function() {
       return {
           info: function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'information',
                   timeout: 5000
               });
           },
           infoFast: function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'information',
                   timeout: 1000
               });
            },
           persistentInfo: function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'information',
                   timeout: 0
               });
           },
           techError: function(text) {
               var message = "";
               if (text) message = text;
               message += " Er is een technisch probleem opgetreden.";
               noty({
                   text: message,
                   layout: 'topCenter',
                   type: 'warning',
                   timeout: 10000
               });
           },
           techErrorFast:function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'warning',
                   timeout: 1000
               });
           },
           error: function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'error',
                   timeout: 5000
               });
           },
           confirm: function(text, successCallBackFunction) {
               noty({
                   text: text,
                   layout: 'center',
                   type: 'information',
                   modal: true,
                   buttons: [
                       {addClass: 'btn btn-primary', text: 'Ok', onClick: function($noty) {
                           successCallBackFunction();
                           $noty.close();
                       }
                       },
                       {addClass: 'btn btn-primary', text: 'Cancel', onClick: function($noty) {
                           $noty.close();
                       }
                       }
                   ]
               });
           },
           success: function(text) {
               noty({
                   text: text,
                   layout: 'topCenter',
                   type: 'success',
                   timeout: null,
                   buttons: [{addClass: 'btn btn-primary', text: 'Yeah!', onClick: function($noty) {
                           $noty.close();
                       }}]
               });
           }
       };
    });



